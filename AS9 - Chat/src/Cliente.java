import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) {
		ChatCliente chat = new ChatCliente("Chat Cliente");
		chat.setVisible(true);
		
		try {
			iniciarCliente(chat);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void iniciarCliente(ChatCliente chat) throws UnknownHostException, IOException {
		// Cliente tenta se conectar ao servidor
		Socket cliente = new Socket("127.0.0.1", 12345);
		System.out.println("O cliente se conectou ao servidor");
		
		// Ler as linhas de entrada do cliente e enviar para buffer de saída
		Scanner teclado = new Scanner(System.in);
		PrintStream saida = new PrintStream(cliente.getOutputStream());
		while(teclado.hasNextLine()){
			saida.println(teclado.nextLine());
		}
		
		saida.println(chat.getMenssagemTexto());
		
		saida.close();
		teclado.close();
		cliente.close();
	}
}
