
// Importa os pacotes necessários
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.*;

/** CLASSE Chat
* ------------------------------------
* Projeto..: default
* Descrição: 
* Direitos.: Fernando Anselmo © mai - 2019
* Autor....: Cafeteira Versão 1.09 - Java - Gerador Automático
* 
*/
public class ChatServidor extends JFrame implements ActionListener {
	
	private Object obj;
	
	private int servidorPorta;
	private String servidorNome;
	private String menssagemTexto;

	private static final long serialVersionUID = 1L;
// Insira aqui os dados da Criação dos Objetos (bloco 1)
// Objetos da Janela (bloco 1)
	private JLabel labelMensagem = new JLabel("Mensagem");
	private JLabel labelServidor = new JLabel("Servidor");
	private JLabel labelNome = new JLabel("Nome");
  private JTextField servidor = new JTextField();
  private JTextField nome = new JTextField();
  private JButton botaoConectar = new JButton("Conectar");
  private JTextArea logMensagens = new JTextArea();
  private JTextField mensagem = new JTextField();
  private JButton botaoEnviar = new JButton("Enviar");

  public ChatServidor(String nomeJanela) {
    try {
    	
  	  add(botaoConectar);
  	  add(servidor);
  	  add(nome);
  	  add(mensagem);
  	  botaoConectar.addActionListener(this);
  	  
      mostra(nomeJanela);
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.obj = e.getSource();
		
		if(obj == botaoConectar){
			this.servidorPorta = Integer.parseInt(this.servidor.getText());
			
			this.servidorNome = this.nome.getText();
			
			
				try {
					this.iniciarServidor(servidorPorta);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
		}
		
		if(obj == mensagem){
			this.menssagemTexto = this.mensagem.getText();
		}
	}
  
  public void iniciarServidor(int porta) throws IOException{
	// Abrir a porta
			ServerSocket servidor = new ServerSocket(porta);			
			this.logMensagens.setText("A porta" + porta + " esta aberta.");
			
			// Esperar e aceitar um cliente
			Socket cliente = servidor.accept();
			System.out.println("Nova conexao com o cliente " + cliente.getInetAddress().getHostAddress());
			
			// Ler as informacoes que o cliente enviar
			Scanner s = new Scanner(cliente.getInputStream());
			while(s.hasNextLine()){
				System.out.println(s.nextLine());
			}
			
			s.close();
			servidor.close();
			cliente.close();
  }

public void mostra(String nomeJanela) throws Exception {

// Insira aqui os dados da Criação da Janela (bloco 2)
// Dados da Janela (bloco 2)
    this.getContentPane().setLayout(null);
    this.getContentPane().setBackground(new Color(238, 238, 238));
    this.setSize(555, 393);
    this.setLocation(232, 185);
    this.setTitle(nomeJanela);
    this.setResizable(false);

// Insira aqui os dados da Criação dos Controles na Janela (bloco 3)
// Cria os Objetos na Janela (bloco 3)
    botaoEnviar.setBounds(new Rectangle(446, 334, 100, 30));
    this.getContentPane().add(botaoEnviar, null);
    mensagem.setBounds(new Rectangle(86, 337, 350, 21));
    this.getContentPane().add(mensagem, null);
    labelMensagem.setBounds(new Rectangle(8, 339, 70, 15));
    this.getContentPane().add(labelMensagem, null);
    logMensagens.setBounds(new Rectangle(10, 31, 530, 300));
    this.getContentPane().add(logMensagens, null);
    labelServidor.setBounds(new Rectangle(15, 7, 57, 13));
    this.getContentPane().add(labelServidor, null);
    labelNome.setBounds(new Rectangle(240, 9, 57, 13));
    this.getContentPane().add(labelNome, null);
    botaoConectar.setBounds(new Rectangle(448, 1, 100, 30));
    this.getContentPane().add(botaoConectar, null);
    servidor.setBounds(new Rectangle(66, 5, 170, 21));
    this.getContentPane().add(servidor, null);
    nome.setBounds(new Rectangle(280, 5, 170, 21));
    this.getContentPane().add(nome, null);

    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        aoFechar();
      }
    });
  }

  private void aoFechar() {
    System.exit(0);
  }

	public JButton getBotaoEnviar() {
		return botaoEnviar;
	}

	public void setBotaoEnviar(JButton botaoEnviar) {
		this.botaoEnviar = botaoEnviar;
	}

	public JTextField getMensagem() {
		return mensagem;
	}

	public void setMensagem(JTextField mensagem) {
		this.mensagem = mensagem;
	}

	public JTextArea getLogMensagens() {
		return logMensagens;
	}

	public void setLogMensagens(JTextArea logMensagens) {
		this.logMensagens = logMensagens;
	}

	public JButton getBotaoConectar() {
		return botaoConectar;
	}

	public void setBotaoConectar(JButton botaoConectar) {
		this.botaoConectar = botaoConectar;
	}

	public JTextField getServidor() {
		return servidor;
	}

	public void setServidor(JTextField servidor) {
		this.servidor = servidor;
	}

	public JTextField getNome() {
		return nome;
	}

	public void setNome(JTextField nome) {
		this.nome = nome;
	}

  public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public int getServidorPorta() {
		return servidorPorta;
	}

	public void setServidorPorta(int servidorPorta) {
		this.servidorPorta = servidorPorta;
	}

	public String getServidorNome() {
		return servidorNome;
	}

	public void setServidorNome(String servidorNome) {
		this.servidorNome = servidorNome;
	}

	public String getMenssagemTexto() {
		return menssagemTexto;
	}

	public void setMenssagemTexto(String menssagemTexto) {
		this.menssagemTexto = menssagemTexto;
	}
  	
}