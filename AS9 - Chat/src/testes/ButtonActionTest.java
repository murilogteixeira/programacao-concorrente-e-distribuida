package testes;

import javax.swing.*;
import java.awt.event.*;
//implemente a interface ActionListener para ouvir os eventos
public class ButtonActionTest extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//declarando os componentes do Frame
	private JButton btn1;
	private JButton btn2;
	private JTextField txt;

	public ButtonActionTest(){
	//instanciando os botões	
	btn1=new JButton("Botao 1");
	btn2=new JButton("Botao 2");
	txt=new JTextField(15);
	//adicionando o ouvinte aos botões
	btn1.addActionListener(this);
	btn2.addActionListener(this);
	JPanel painel2=new JPanel();
	painel2.add(txt);
	JPanel painel1=new JPanel();
	painel1.add(btn1);
	painel1.add(btn2);
	add(painel2,"North");
	add(painel1,"Center");
	setVisible(true);
	pack();
	setDefaultCloseOperation(EXIT_ON_CLOSE);
}
	//método que trata os eventos
	public void actionPerformed(ActionEvent evt) {
		Object obj=evt.getSource();
	if(obj==btn1){
		txt.setText("Foi pressionado o botão 1");
	}
	if(obj==btn2){
		txt.setText("Foi pressionado o botão 2");
	}
	}
	public static void main(String[] args){
		new ButtonActionTest();
	}
}