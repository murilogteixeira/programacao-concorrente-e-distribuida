import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

	public static void main(String[] args) throws IOException {
		executarServidor(12345);
		ChatServidor chat = new ChatServidor("Chat Servidor");
		chat.setVisible(true);
	}

	public static void executarServidor(int porta) throws IOException {
		// Abrir a porta
		ServerSocket servidor = new ServerSocket(porta);
		System.out.println("A porta 12345 esta aberta.");
		
		// Esperar e aceitar um cliente
		Socket cliente = servidor.accept();
		System.out.println("Nova conexao com o cliente " + cliente.getInetAddress().getHostAddress());
		
		// Ler as informacoes que o cliente enviar
		Scanner s = new Scanner(cliente.getInputStream());
		while(s.hasNextLine()){
			System.out.println(s.nextLine());
		}

		while(chat.isActive()){
			chat.getLogMensagens().setText(s.nextLine());
		}
		s.close();
		servidor.close();
		cliente.close();
	}
	
}
