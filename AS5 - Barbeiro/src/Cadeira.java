
public class Cadeira {
	private Boolean disponivel = true;
	private Cliente cliente;
	
	public Cadeira(Boolean disponivel, Cliente cliente) {
		// TODO Auto-generated constructor stub
		setDisponivel(disponivel);
		setCliente(cliente);
		System.out.println("Cadeira criada");
	}

	public Boolean getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(Boolean disponivel) {
		this.disponivel = disponivel;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
