import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Barbeiro
 * 	Acordado
 * 	Dormir
 * 	Cortarcabelo
 * @author Murilo
 *
 */

public class Barbeiro extends Thread {
	private String nome;
	private Barbearia barbearia;
	private Cadeira cadeira;
	private Lock lock = new ReentrantLock();
	
	
	public Barbeiro(Barbearia barbearia, Cadeira cadeira, String nome) {
		setBarbearia(barbearia);
		setCadeira(cadeira);
		setNome(nome);
		System.out.println(getNome() + " criado");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			if(getCadeira().getDisponivel()){
				cortarCabelo();
			}
		}
	}
	
	public void dormir(){
		try {
			this.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cortarCabelo(){
		this.lock.lock();
		try {
			System.out.println(getCadeira().getCliente().getNome() + " cortando o cabelo");
			while(getCadeira().getCliente().getCabelo() > 10){
				getCadeira().getCliente().perderCabelo();
			}
		} finally {
			this.lock.unlock();
		}
		
		
		getCadeira().setCliente(null);
		getCadeira().setDisponivel(true);
		
		int clientesEsperando = 0;
		for (Cadeira cadeira : barbearia.getCadeirasEspera()) {
			if(!cadeira.getDisponivel()){
				getCadeira().setCliente(cadeira.getCliente());
				getCadeira().setDisponivel(false);
				clientesEsperando++;
			}
		}
		if(clientesEsperando == 0){
			dormir();
		}
	}

	public Barbearia getBarbearia() {
		return barbearia;
	}

	public void setBarbearia(Barbearia barbearia) {
		this.barbearia = barbearia;
	}

	public Cadeira getCadeira() {
		return cadeira;
	}

	public void setCadeira(Cadeira cadeira) {
		this.cadeira = cadeira;
	}

	
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
