/*	ProblemadoBarbeiroDorminhoco:
	-Uma barbearia tem um certo n�mero de barbeiros e um certo n�mero de cadeiras para os clientes esperarem
	-Sempre que um barbeiro n�o tem clientes para atender ele aproveita para dormir um pouco
	-Ao chegar � barbearia o cliente tem que acordar um barbeiro para lhe cortar o cabelo
	-Se, entretanto, chegarem mais clientes e todos os barbeiros estiverem ocupados estes devem esperar sentados(se existirem cadeiras livres)
		ou ent�o deixar a barbearia (se todas as cadeiras estiverem ocupadas)

*/
/*
Main
Barbearia
	Barbeiros
	Cadeiras
Barbeiro
	Acordado
	Dormir
	Cortarcabelo
Cliente
	Acordar barbeiro
	Esperar
	Cabelo
	CrescerCabelo
	perderCabelo
*/

public class Main {

	public static void main(String[] args) {
		
		int qtdCadeirasBarbeiro = 3;
		int qtdCadeirasEspera = 10;

		Barbearia barbearia = new Barbearia();

		// criar cadeiras barbeiro
		for (int i = 0; i < qtdCadeirasBarbeiro; i++){
			barbearia.setCadeirasBarbeiros(new Cadeira(false, null));
		}
		
		// criar cadeiras espera
		for (int i = 0; i < qtdCadeirasEspera; i++){
			barbearia.setCadeirasEspera(new Cadeira(false, null));
		}
		
		//criar barbeiros
		for (int i = 0; i < qtdCadeirasBarbeiro; i++){
			barbearia.setBarbeiro(new Barbeiro(barbearia, barbearia.getCadeirasBarbeiros().get(i), "Barbeiro - " + (i + 1)));
			barbearia.getBarbeiros().get(i).start();
		}
		
		for(int i = 0; i < 10; i++){
			barbearia.entrarCliente(new Cliente("Cliente - " + (i+1)));
		}
		
	}

}
