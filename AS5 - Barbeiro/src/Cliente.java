/**
 * 
 * Cliente
 *  	Acordar barbeiro
 *  	Esperar
 *  	Cabelo
 *  	perderCabelo
 * @author Murilo
 *
 */
public class Cliente {
	private String nome;
	private int cabelo = 1000;
	
	public Cliente(String nome) {
		// TODO Auto-generated constructor stub
		setNome(nome);
		System.out.println(getNome() +" criado");
	}
	
	
	public void esperar(){
		try {
			this.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void perderCabelo(){
		setCabelo(cabelo-10);
		System.out.println("Perdendo cabelo");
	}
	
	public void acordarBarbeiro(Barbeiro barbeiro){
		barbeiro.notifyAll();
	}

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public int getCabelo() {
		return cabelo;
	}

	public void setCabelo(int cabelo) {
		this.cabelo = cabelo;
	}
}
