import java.util.ArrayList;

public class Barbearia {

	private ArrayList<Cadeira> cadeirasBarbeiros = new ArrayList<>();
	private ArrayList<Cadeira> cadeirasEspera = new ArrayList<>();
	private ArrayList<Barbeiro> barbeiros = new ArrayList<>();
	
	public Barbearia() {
		System.out.println("Barbearia criada");
	}
	
	public void entrarCliente(Cliente cliente){
		System.out.println(cliente.getNome() + " entrou na barbearia");
		int cadeirasBarbeiroDisponiveis = 0;
		int cadeirasEsperaDisponiveis = 0;
		for (Barbeiro barbeiro : barbeiros) {
			if(barbeiro.getCadeira().getDisponivel()){
				cadeirasBarbeiroDisponiveis++;
				barbeiro.getCadeira().setDisponivel(false);;
				barbeiro.getCadeira().setCliente(cliente);
				barbeiro.getCadeira().getCliente().acordarBarbeiro(barbeiro);
			}
		}
		if(cadeirasBarbeiroDisponiveis == 0){
			for (Cadeira cadeira : cadeirasEspera) {
				if(cadeira.getDisponivel()){
					cadeirasEsperaDisponiveis++;
					cadeira.setCliente(cliente);
					cliente.esperar();
				}
			}
		}
		if(cadeirasEsperaDisponiveis == 0){
			//cliente vai embora
		}

		System.out.println(cliente.getNome() + " saiu da barbearia");
	}

	public ArrayList<Cadeira> getCadeirasBarbeiros() {
		return cadeirasBarbeiros;
	}

	public void setCadeirasBarbeiros(Cadeira cadeiraBarbeiro) {
		this.cadeirasBarbeiros.add(cadeiraBarbeiro);
	}

	public ArrayList<Cadeira> getCadeirasEspera() {
		return cadeirasEspera;
	}

	public void setCadeirasEspera(Cadeira cadeiraEspera) {
		this.cadeirasEspera.add(cadeiraEspera);
	}

	public ArrayList<Barbeiro> getBarbeiros() {
		return barbeiros;
	}

	public void setBarbeiro(Barbeiro barbeiro) {
		this.barbeiros.add(barbeiro);
	}

	
}
