/**
 * 3.Problema  dos  fumantes:  três  fumantes  e  um  agente  sentados  em  uma  mesa. 
 * Cada  fumante possui  dois  dos  três  ingredientes  para  se  fazer  um  cigarro: 
 * fósforo,  fumo  e  palha.  O  agente possui os três e aleatoriamente sorteia um dos ingredientes.
 * O fumante contemplado faz o seu cigarro, fuma e libera o agente para fazer novo sorteio.
 * 
 * @author murilo
 *
 */
public class Main {
	public static void main(String[] args) {
		Agente ag = new Agente();
		
		Fumante f1 = new Fumante("Fumante 1", ag);
		Fumante f2 = new Fumante("Fumante 2", ag);
		Fumante f3 = new Fumante("Fumante 3", ag);
		
		f1.setFosforo(false);
		f1.setFumo(true);
		f2.setPalha(true);

		f2.setFosforo(true);
		f1.setFumo(false);
		f2.setPalha(true);

		f1.setFosforo(true);
		f3.setPalha(true);
		f3.setFumo(false);
		
		f1.start();
		f2.start();
		f3.start();
		
		
	}
}
