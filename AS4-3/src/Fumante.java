
public class Fumante extends Thread{
	private Boolean fosforo = null;
	private Boolean palha = null;
	private Boolean fumo = null;
	private String nome = "";
	private Agente agente;
	
	public Fumante(String nome, Agente agente) {
		this.setNome(nome);
		this.setAgente(agente);
	}
	
	@Override
	public void run() {
		super.run();
		
		while(true){
			this.pegarIngrediente();
		}
	}
	
	public synchronized void pegarIngrediente(){
			int numSorteio = this.getAgente().sorteio();
			Ingrediente ingrediente = this.agente.pegarIngrediente(numSorteio);
			this.fumar(numSorteio, ingrediente);
			try {
				this.notify();
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public void fumar(int numSorteio, Ingrediente ingrediente){
		switch (ingrediente.getNome()) {
		case "fumo":
			if(this.fumo == false){
				this.getAgente().setIngredientes(numSorteio, false);
				System.out.println(this.getNome() + " pegou o " + ingrediente.getNome());
				System.out.println(this.getNome() + " Fumando...");
				try {
					Fumante.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(this.getNome() + " devolveu o " + ingrediente.getNome());
				this.getAgente().setIngredientes(numSorteio, true);
			}
			break;
			
		case "fosforo":
			if(this.fosforo == false){
				this.getAgente().setIngredientes(numSorteio, false);
				System.out.println(this.getNome() + " pegou o " + ingrediente.getNome());
				System.out.println(this.getNome() + " Fumando...");
				try {
					Fumante.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(this.getNome() + " devolveu o " + ingrediente.getNome());
				this.getAgente().setIngredientes(numSorteio, true);
			}
			break;
			
		case "palha":
			if(this.palha == false){
				this.getAgente().setIngredientes(numSorteio, false);
				System.out.println(this.getNome() + " pegou o " + ingrediente.getNome());
				System.out.println(this.getNome() + " Fumando...");
				try {
					Fumante.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(this.getNome() + " devolveu o " + ingrediente.getNome());
				this.getAgente().setIngredientes(numSorteio, true);
			}
			break;

		default:
			break;
		}
	}

	public Boolean getFosforo() {
		return fosforo;
	}

	public void setFosforo(Boolean fosforo) {
		this.fosforo = fosforo;
	}

	public Boolean getPalha() {
		return palha;
	}

	public void setPalha(Boolean palha) {
		this.palha = palha;
	}

	public Boolean getFumo() {
		return fumo;
	}

	public void setFumo(Boolean fumo) {
		this.fumo = fumo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	
}
