import java.util.ArrayList; 
import java.util.Random;

public class Agente {
	private ArrayList<Ingrediente> ingredientes = new ArrayList<>();
	
	public Agente() {
		ingredientes.add(new Ingrediente("palha", true));
		ingredientes.add(new Ingrediente("fumo", true));
		ingredientes.add(new Ingrediente("fosforo", true));
	}

	public ArrayList<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(int num, Boolean valor) {
		this.ingredientes.get(num).setAtivo(valor);
	}
	
	public int sorteio(){
		int num = new Random().nextInt(3);
		System.out.println("Numero " + num + " sorteado");
		return num;
	}
	
	public Ingrediente pegarIngrediente(int num){
		Ingrediente i = ingredientes.get(num);
		System.out.println("Ingrediente sorteado - " + i.getNome());
		return i;
	}

}
