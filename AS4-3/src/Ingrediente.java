
public class Ingrediente {
	private String nome;
	private Boolean ativo;
	
	public Ingrediente(String nome, Boolean ativo) {
		this.setNome(nome);
		this.setAtivo(ativo);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
}
