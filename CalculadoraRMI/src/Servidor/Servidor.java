package Servidor;

import java.rmi.Naming;

public class Servidor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			CalculadoraRMI calc = new CalculadoraRMI();
//			Naming.rebind("calc", calc);
			Naming.rebind("rmi://localhost:1099/calc", calc);
			System.out.println("Registrado.");
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
