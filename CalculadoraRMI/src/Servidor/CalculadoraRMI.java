package Servidor;

import java.rmi.*;
import java.rmi.server.*;

public class CalculadoraRMI extends UnicastRemoteObject implements Calculadora {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected CalculadoraRMI() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int somar(int op1, int op2) throws RemoteException {
		// TODO Auto-generated method stub
		return op1 + op2;
	}

	@Override
	public int subtrair(int op1, int op2) throws RemoteException {
		// TODO Auto-generated method stub
		return op1 - op2;
	}

	@Override
	public int dividir(int op1, int op2) throws RemoteException {
		// TODO Auto-generated method stub
		return op1 / op2;
	}

	@Override
	public int multiplicar(int op1, int op2) throws RemoteException {
		// TODO Auto-generated method stub
		return op1 * op2;
	}
	
	
	
}
