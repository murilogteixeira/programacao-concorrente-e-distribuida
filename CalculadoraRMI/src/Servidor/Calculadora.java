package Servidor;

import java.rmi.*;

public interface Calculadora extends Remote {
	public int somar(int op1, int op2) throws RemoteException;
	public int subtrair(int op1, int op2) throws RemoteException;
	public int dividir(int op1, int op2) throws RemoteException;
	public int multiplicar(int op1, int op2) throws RemoteException;
}
