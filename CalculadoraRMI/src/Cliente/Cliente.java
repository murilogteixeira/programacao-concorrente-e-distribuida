package Cliente;

import java.io.*;
import java.rmi.Naming;

import Servidor.Calculadora;

public class Cliente {

	public static void main(String[] args) {
		try {
			Calculadora calc = (Calculadora)Naming.lookup("rmi://localhost:1099/calc");
			int result = calc.dividir(100, 3);
			System.out.println("O resultado da operacao eh: " + result);
			
//			BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in))
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
