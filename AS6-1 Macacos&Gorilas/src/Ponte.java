import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ponte {
	public int contadorDireitaMacaco;
	public int contadorEsquerdaMacaco;
	public int contadorDireitaGorila;
	public int contadorEsquerdaGorila;
	public Lock botao;
	
	public Ponte() {
		// TODO Auto-generated constructor stub
		this.contadorDireitaMacaco = 0;
		this.contadorEsquerdaMacaco = 0;
		this.contadorDireitaGorila = 0;
		this.contadorEsquerdaGorila = 0;
		this.botao = new ReentrantLock();
		System.out.println("Ponte criada");
	}

	public void somarPlacaMacaco(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorDireitaMacaco++;
				System.out.println("Placa lado direito: "+ this.contadorDireitaMacaco);
				break;

			case "esq":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorEsquerdaMacaco++;
				System.out.println("Placa lado esquerdo: "+ this.contadorEsquerdaMacaco);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}

	public void subtrairPlacaMacaco(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorDireitaMacaco--;
				System.out.println("Placa lado direito: "+ this.contadorDireitaMacaco);
				break;

			case "esq":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorEsquerdaMacaco--;
				System.out.println("Placa lado direito: "+ this.contadorEsquerdaMacaco);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}
	
	public void somarPlacaGorila(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorDireitaGorila++;
				System.out.println("Placa lado direito: "+ this.contadorDireitaGorila);
				break;

			case "esq":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorEsquerdaGorila++;
				System.out.println("Placa lado esquerdo: "+ this.contadorEsquerdaGorila);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}

	public void subtrairPlacaGorila(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorDireitaGorila--;
				System.out.println("Placa lado direito: "+ this.contadorDireitaGorila);
				break;

			case "esq":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorEsquerdaGorila--;
				System.out.println("Placa lado direito: "+ this.contadorEsquerdaGorila);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}

}

