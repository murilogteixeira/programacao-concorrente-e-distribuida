import java.util.Random;

public class Gorila extends Thread{
	private String lado;
	private Ponte ponte;
	//private String especie; 
	
	public Gorila(String nome, String lado, Ponte ponte) {
		super(nome);
		this.lado = lado;
		this.ponte = ponte;
		//this.especie = "Gorila";
		this.setPriority(5);
		System.out.println(this.getName() + " criado");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		
		while(true){
			this.verificaLado();
			this.comer();
		}
		
	}
	
	public void verificaLado(){
		String irParaLado = "";
		
		if(this.lado == "dir"){
			irParaLado = "esq";
		}else{
			irParaLado = "dir";
		}
		
		switch (irParaLado) {
		case "dir":
			while(this.ponte.contadorEsquerdaMacaco > 0 || this.ponte.contadorEsquerdaGorila > 0);//{
			this.atravessar(irParaLado);
//				try {
//					this.wait();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}else{
//				this.atravessar(irParaLado);
//			}
			break;

		case "esq":
			while(this.ponte.contadorDireitaMacaco > 0 || this.ponte.contadorDireitaGorila > 0);//{
			this.atravessar(irParaLado);
//				try {
//					this.wait();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}else{
//				this.atravessar(irParaLado);
//			}
			break;

		default:
			break;
		}
	}
	
	public void atravessar(String irParaLado){
		try {
			this.ponte.somarPlacaGorila(irParaLado);
			this.ponte.somarPlacaMacaco(irParaLado);
			System.out.println(this.getName() + " atravessando a ponte para o lado " + irParaLado);
			Macaco.sleep(new Random().nextInt(5000));
			this.ponte.subtrairPlacaGorila(irParaLado);
			this.ponte.subtrairPlacaMacaco(irParaLado);
			System.out.println(this.getName() + " atravessou a ponte");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(this.lado == "dir"){
			this.lado = "esq";
		}else{
			this.lado = "dir";
		}
		
		System.out.println(this.getName() + " est� no lado " + this.lado);
	}
	
	public void comer(){
		try {
			System.out.println(this.getName() + " comendo");
			Macaco.sleep(new Random().nextInt(8000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
