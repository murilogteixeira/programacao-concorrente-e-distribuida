package RMI;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import natal.MuriloRMI;

public class ServidorAjudante2 {

	public static void main(String[] args) throws RemoteException, MalformedURLException {
		MuriloRMI murilo = new MuriloRMI();
		Naming.rebind("rmi://localhost:1099/narizVermelho", murilo);
		System.out.println("Registrado");
	}

}
