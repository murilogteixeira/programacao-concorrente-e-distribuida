package Cliente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;

import natal.Mensagem;
import natal.Murilo;

public class ClienteAjudante1 extends Thread {

	public static void main(String[] args) throws IOException, NotBoundException, InterruptedException {
		// UDP
		byte[] entrada = new byte[1000], saida = "OK".getBytes();
		DatagramSocket socket = new DatagramSocket();
		DatagramPacket dgSaida, dgEntrada;
		dgSaida = new DatagramPacket(saida, saida.length, InetAddress.getByName("localhost"), 12345);
		socket.send(dgSaida);
		dgEntrada = new DatagramPacket(entrada, entrada.length);
		socket.receive(dgEntrada);
		socket.close();
		
		// Obter o nome e o presente da string recebida
		String[] crianca = new String(entrada).split(";");
		
		// RMI
		Murilo murilo = (Murilo)Naming.lookup("rmi://localhost:1099/narizVermelho");

		// TCP
		Socket socketTCP = new Socket("localhost", 12346);
		ObjectOutputStream saidaTCP = new ObjectOutputStream(socketTCP.getOutputStream()); 
		ClienteAjudante1.sleep(2);
		saidaTCP.writeObject(new Mensagem(crianca[0], murilo.seComportou(crianca[0]), crianca[1]));
		socketTCP.close();
		saidaTCP.close();
	}

}
