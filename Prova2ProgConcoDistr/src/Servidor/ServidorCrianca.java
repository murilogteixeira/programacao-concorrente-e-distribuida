package Servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

import natal.Mensagem;

public class ServidorCrianca {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// UDP
		String nomePresente = "murilo;PS4";
		byte[] entrada = new byte[1000], saida = nomePresente.getBytes();
		DatagramSocket socketUDP = new DatagramSocket(12345);
		System.out.println("===> Crian�a conectada UDP");
		DatagramPacket dgSaida, dgEntrada;
		dgEntrada = new DatagramPacket(entrada, entrada.length);
		socketUDP.receive(dgEntrada);
		System.out.println("-> Cliente conectado UDP: " + dgEntrada.getAddress().getHostAddress() + ":" + dgEntrada.getPort());
		dgSaida = new DatagramPacket(saida, saida.length, dgEntrada.getAddress(), dgEntrada.getPort());
		socketUDP.send(dgSaida);
		socketUDP.close();
		System.out.println("===> Crian�a desconectada UDP");
		
		System.out.println();
		// TCP
		ServerSocket servidor = new ServerSocket(12346);
		System.out.println("===> Crian�a conectada TCP");
		Socket socketTCP = servidor.accept();
		System.out.println("-> Cliente conectado TCP: " + socketTCP.getInetAddress().getHostAddress() + ":" + socketTCP.getPort());
		ObjectInputStream entradaTCP = new ObjectInputStream(socketTCP.getInputStream());
		Mensagem mensagem = (Mensagem)entradaTCP.readObject();
		System.out.println("> Resposta:\nNome: " + mensagem.getNomeCrianca() + "\nVai ganhar? " + mensagem.getVaiGanharPresente() + "\nPresente desejado: " + mensagem.getPresenteDesejado());
		servidor.close();
		entradaTCP.close();
		System.out.println("===> Crian�a desconectada TCP");
	}

}
