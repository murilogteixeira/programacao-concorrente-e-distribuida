package natal;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

public class MuriloRMI extends UnicastRemoteObject implements Murilo {
	private static final long serialVersionUID = 1L;

	public MuriloRMI() throws RemoteException {
		super();
	}

	@Override
	public boolean seComportou(String nome) throws RemoteException {
		return new Random().nextBoolean();
	}

}
