package natal;

import java.io.Serializable;

public class Mensagem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nomeCrianca;
	private Boolean vaiGanharPresente;
	private String presenteDesejado;
	
	public Mensagem(String nome, Boolean vaiGanhar, String presente){
		this.nomeCrianca = nome;
		this.vaiGanharPresente = vaiGanhar;
		this.presenteDesejado = presente;
	}
	
	public String getNomeCrianca() {
		return nomeCrianca;
	}
	public void setNomeCrianca(String nomeCrianca) {
		this.nomeCrianca = nomeCrianca;
	}
	public Boolean getVaiGanharPresente() {
		return vaiGanharPresente;
	}
	public void setVaiGanharPresente(Boolean vaiGanharPresente) {
		this.vaiGanharPresente = vaiGanharPresente;
	}
	public String getPresenteDesejado() {
		return presenteDesejado;
	}
	public void setPresenteDesejado(String presenteDesejado) {
		this.presenteDesejado = presenteDesejado;
	}
	
}
