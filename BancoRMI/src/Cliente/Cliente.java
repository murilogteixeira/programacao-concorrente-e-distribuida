package Cliente;

import java.io.Serializable;
import java.rmi.Naming;

import Default.*;

public class Cliente implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		try {
			Banco banco = (Banco)Naming.lookup("rmi://localhost:1099/Banco");
			
			// Criar nova conta e obter a chave
			ChaveAcesso chave = banco.abrirConta("Murilo");
			if(chave != null) {
				System.out.println("Conta criada.");
			} else {
				System.out.println("Problema ao criar conta.");
			}
			
			// Obter o numero da conta
			Integer numero = banco.getNumeroConta(chave);
			
			// Se ja tiver conta, criar a chave de acesso
//			ChaveAcesso chave = new ChaveAcesso(1774860792, "Murilo");
//			Integer numero = 1;
			
			Boolean depositar = banco.depositar(numero, 100, chave);
			System.out.println("Deposito efetuado: " + depositar);
			
			Boolean sacar = banco.sacar(numero, 50, chave);
			System.out.println("Saque efetuado: " + sacar);
			
			Double saldo = banco.getSaldo(numero, chave);
			System.out.println("Saldo: R$" + saldo);
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
