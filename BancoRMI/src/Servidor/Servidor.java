package Servidor;

import java.rmi.Naming;

public class Servidor {

	public static void main(String[] args) {
		try {
			BancoRMI banco = new BancoRMI();
			Naming.rebind("rmi://localhost:1099/Banco", banco);
			System.out.println("Banco Registrado.");
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
