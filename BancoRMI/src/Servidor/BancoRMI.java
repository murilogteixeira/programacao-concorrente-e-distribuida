package Servidor;


import java.rmi.*;
import java.rmi.server.*;
import java.util.Vector;
import Default.*;

public class BancoRMI extends UnicastRemoteObject implements Banco {
	private static final long serialVersionUID = 1L;
	
	Integer numeroUltimaConta;
	Vector<Conta> contas;
	
	protected BancoRMI() throws RemoteException {
		super();
		numeroUltimaConta = 0;
		contas = new Vector<Conta>();
		System.out.println("Banco iniciado");
	}

	@Override
	public ChaveAcesso abrirConta(String nome) {
		
		// Criar chave
		Boolean chaveExiste = false;
		ChaveAcesso chave = null;
		do {
			chaveExiste = false;
			chave = new ChaveAcesso();
			for (Conta conta : contas) {
				if (conta.getChave().getChaveAcesso() == chave.getChaveAcesso()) {
					chaveExiste = true;
				}
			}
		} while(chaveExiste);
		
		// Criar conta
		contas.add(new Conta(chave, ++numeroUltimaConta, nome));
		
		// Retornar chave de acesso
		return chave;
	}

	@Override
	public Integer getNumeroConta(ChaveAcesso chave) {
				
		// Verificar se a chave de acesso existe
		for (Conta conta : contas) {
			if(conta.getChave().getChaveAcesso().equals(chave.getChaveAcesso())) {
				// Retornar o numero da conta
				return conta.getNumero();
			}
		}
		
		return null;
	}

	@Override
	public boolean depositar(Integer numeroConta, float valor, ChaveAcesso chave) {
				
		// Verificar se a chave de acesso e a conta existem
		for (Conta conta2 : contas) {
			
			if(conta2.getNumero().equals(numeroConta)) {
				
				if(conta2.getChave().getChaveAcesso().equals(chave.getChaveAcesso())) {
					// Depositar
					conta2.setSaldo(conta2.getSaldo() + valor);
					return true;
				}
			}
		}
		
		return false;
	}

	@Override
	public boolean sacar(Integer numeroConta, float valor, ChaveAcesso chave) {
		
		// Verificar se a chave de acesso e a conta existem
		for (Conta conta2 : contas) {
			
			if(conta2.getNumero().equals(numeroConta)) {
				
				if(conta2.getChave().getChaveAcesso().equals(chave.getChaveAcesso())) {
					// Sacar
					if(conta2.getSaldo() >= valor) {
						conta2.setSaldo(conta2.getSaldo() - valor);
						return true;
					}
				}
			}
		}
		
		return false;
	}

	@Override
	public Double getSaldo(Integer numeroConta, ChaveAcesso chave) {
		
		// Verificar se a chave de acesso e a conta existem
		for (Conta conta2 : contas) {
			
			if(conta2.getNumero().equals(numeroConta)) {
				
				if(conta2.getChave().getChaveAcesso().equals(chave.getChaveAcesso())) {
					// Obter saldo
					return conta2.getSaldo();
				}
			}
		}

		return null;
	}

	@Override
	public boolean fecharConta(Integer numeroConta, ChaveAcesso chave) {
		
		// Verificar se a chave de acesso e a conta existem
		for (Conta conta2 : contas) {
			
			if(conta2.getNumero().equals(numeroConta)) {
				
				if(conta2.getChave().getChaveAcesso().equals(chave.getChaveAcesso())) {
					// Fechar conta
					contas.remove(conta2);
					return true;
				}
			}
		}
		
		
		// Retornar se foi possível sacar
		return false;
	}

}
