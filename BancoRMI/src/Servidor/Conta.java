package Servidor;

import java.io.Serializable;

import Default.ChaveAcesso;

public class Conta implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ChaveAcesso chave;
	private Integer numero;
	private String nome;
	private Double saldo;
	
	public Conta(ChaveAcesso chave, Integer numero, String nome) {
		this.chave = chave;
		this.numero = numero;
		this.nome = nome;
		this.saldo = 0.0;
		System.out.println("--- Conta Criada. ---\n"
				+ "Nome: " + nome + "\n"
				+ "Numero: " + numero + "\n"
				+ "Chave: " + chave.getChaveAcesso() + "\n");
	}

	public ChaveAcesso getChave() {
		return chave;
	}

	public void setChave(ChaveAcesso chave) {
		this.chave = chave;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	
}
