package Default;
import java.io.Serializable;
import java.util.Random;

public class ChaveAcesso implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer chaveAcesso;

	public ChaveAcesso() {
		criarChaveAcesso();
	}
	
	public ChaveAcesso(Integer chave) {
		this.chaveAcesso = chave;
	}
	
	private void criarChaveAcesso() {
		this.chaveAcesso = new Random().nextInt(1999999999);
	}
	
	public Integer getChaveAcesso() {
		return chaveAcesso;
	}
}
