package Default;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Banco extends Remote {
	public ChaveAcesso abrirConta(String nome) throws RemoteException;
	public Integer getNumeroConta(ChaveAcesso chave) throws RemoteException;
	public boolean depositar(Integer numeroConta, float valor, ChaveAcesso chave) throws RemoteException;
	public boolean sacar(Integer numeroConta, float valor, ChaveAcesso chave) throws RemoteException;
	public Double getSaldo(Integer numeroConta, ChaveAcesso chave) throws RemoteException;
	public boolean fecharConta(Integer numeroConta, ChaveAcesso chave) throws RemoteException;
}
