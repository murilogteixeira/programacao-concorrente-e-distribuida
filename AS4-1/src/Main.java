/**
 * 1.Problema  do  banheiro  unissex  [Andrews  91]:  Suponha  que  existe  apenas  um  banheiro, 
 * que pode  ser  visitado  por  homens  e  mulheres,  mas  não  simultaneamente.  Quando  o  banheiro estiver  vazio, 
 * tanto  um  homem  quanto uma  mulher  podem  entrar.  Se  houver  um  homem  no banheiro  outros  cavalheiros  poderão 
 * entrar  e  quando  houver  uma  mulher  no  banheiro  outras continuarão a ser admitidas.  Quando a última mulher sair
 * deverá dar prioridade a um homem e vice versa.
 */

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Banheiro banheiro = new Banheiro();
		ArrayList<Homem> homens = new ArrayList<>();
		ArrayList<Mulher> mulheres = new ArrayList<>();
		
		for(int i = 0; i < 5; i++){
			homens.add(new Homem("Homem " + (i + 1), "M", banheiro));
			mulheres.add(new Mulher("Mulher " + (i + 1), "F", banheiro));

			homens.get(i).start();
			mulheres.get(i).start();
		}
		
	}

}
