import java.util.ArrayList;

public class Banheiro {
	//ATRIBUTOS-------------------------
	private ArrayList<Pessoa> homens = new ArrayList<>();
	private ArrayList<Pessoa> mulheres = new ArrayList<>();
	
	//CONSTRUTORES-------------------------
	public Banheiro() {
		
	}
	
	//METODOS-------------------------
	public int getQtdHomens() {
		return this.homens.size();
	}

	public int getQtdMulheres() {
		return this.mulheres.size();
	}

	public ArrayList<Pessoa> getHomens() {
		return homens;
	}

	public void entraHomem(Pessoa homem) {
		System.out.println(homem.getNome() + " - entrou no banheiro");
		this.homens.add(homem);
		System.out.println(this.getQtdHomens() + " homens no banheiro");
	}

	public void sairHomem(Pessoa homem) {
		System.out.println(homem.getNome() + " - saiu no banheiro");
		this.homens.remove(homem);
		System.out.println(this.getQtdHomens() + " homens no banheiro");
		if(this.getQtdHomens() == 0){
			System.out.println("O banheiro está desocupado. Preferência das mulheres.");
			homem.notify();
		}
	}

	public ArrayList<Pessoa> getMulheres() {
		return mulheres;
	}

	public void entraMulher(Pessoa mulher) {
		System.out.println(mulher.getNome() + " - entrou no banheiro");
		this.mulheres.add(mulher);
		System.out.println(this.getQtdMulheres() + " mulheres no banheiro");
	}

	public void sairMulher(Pessoa mulher) {
		System.out.println(mulher.getNome() + " - saiu no banheiro");
		this.mulheres.remove(mulher);
		System.out.println(this.getQtdMulheres() + " mulheres no banheiro");
		if(this.getQtdMulheres() == 0){
			System.out.println("O banheiro está desocupado. Preferência dos homens.");
			mulher.notify();
		}
	}
	
	public void entrarBanheiro(Pessoa pessoa){
		switch (pessoa.getGenero()) {
		case "F":
			entraMulher(pessoa);
			break;

		case "M":
			entraHomem(pessoa);
			break;

		default:
			break;
		}
	}
	
	public void sairBanheiro(Pessoa pessoa){
		switch (pessoa.getGenero()) {
		case "F":
			sairMulher(pessoa);
			break;

		case "M":
			sairHomem(pessoa);
			break;

		default:
			break;
		}
	}
	
	public Boolean temHomem(){
		return this.getQtdHomens() > 0;
	}
	
	public Boolean temMulher(){
		return this.getQtdMulheres() > 0;
	}
}
