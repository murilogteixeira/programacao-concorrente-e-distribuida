public class Homem extends Pessoa {

	public Homem(String nome, String genero, Banheiro banheiro) {
		super(nome, genero, banheiro);
		
	}
	
	@Override
	public void run() {
		super.run();
		while(true){
			this.entraBanheiro();
			this.saiBanheiro();
		}
	}

	public synchronized void entraBanheiro(){
		try {
			if(super.getBanheiro().temMulher()){
				System.out.println(this.getNome() + " - banheiro ocupado por mulheres. Aguardando desocupar.");
				this.setPriority(10);
				this.wait(2000);
				this.entraBanheiro();
			}else{
				System.out.println(this.getNome() + " - entrando no banheiro");
				super.getBanheiro().entrarBanheiro(this);
				Mulher.sleep(3000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void saiBanheiro(){

		System.out.println(this.getNome() + " - saindo do banheiro");
		super.getBanheiro().sairBanheiro(this);
		this.setPriority(1);
		try {
			Homem.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
