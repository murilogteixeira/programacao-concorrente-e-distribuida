
public abstract class Pessoa extends Thread{
	private String genero;
	private String nome;
	private Banheiro banheiro;
	
	public Pessoa(String nome, String genero, Banheiro banheiro) {
		setNome(nome);
		setGenero(genero);
		setBanheiro(banheiro);
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Banheiro getBanheiro() {
		return banheiro;
	}

	public void setBanheiro(Banheiro banheiro) {
		this.banheiro = banheiro;
	}
}
