import java.util.*;
 
public class CountingSort{
	static int cont = 0;
 
  public static int[] sort(int[] array) {
    
    int[] aux = new int[array.length];
 
    int min = array[0];
    int max = array[0];
    
	   cont++;
    for (int i = 1; i < array.length; i++) {
 	   cont++;
      if (array[i] < min) {
   	   cont++;
        min = array[i];
 	   cont++;
      } else if (array[i] > max) {
   	   cont++;
        max = array[i];
      }
    }
 
    int[] counts = new int[max - min + 1];
 
	   cont++;
    for (int i = 0;  i < array.length; i++) {
 	   cont++;
      counts[array[i] - min]++;
    }
 
    counts[0]--;
	   cont++;
    for (int i = 1; i < counts.length; i++) {
 	   cont++;
      counts[i] = counts[i] + counts[i-1];
    }
 
	   cont++;
    for (int i = array.length - 1; i >= 0; i--) {
 	   cont++;
        aux[counts[array[i] - min]--] = array[i];
    }
 
    return aux;
  }
 
  public static void main(String[] args) {
 
    int [] arr = {5,3,0,2,4,1,0,5,2,3,1,4}; 
    System.out.println("Counting");
    System.out.println("Antes -->->->->\n " + Arrays.toString(arr));
 
    arr = sort(arr);
   System.out.println("Depois -->->->->\n " + Arrays.toString(arr));
 
    System.out.println("Operações básicas: " + cont);
  }
}