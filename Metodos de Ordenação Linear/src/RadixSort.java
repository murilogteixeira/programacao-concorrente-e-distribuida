import java.util.*; 

class RadixSort { 
	static int cont = 0;

	static int getMax(int arr[], int n) {
		int mx = arr[0]; 
		for (int i = 1; i < n; i++) 
			if (arr[i] > mx) 
				mx = arr[i]; 
		return mx; 
	} 

	static void countSort(int arr[], int n, int exp) {
		int output[] = new int[n]; // output array 
		int i; 
		int count[] = new int[10]; 
		Arrays.fill(count,0); 

		   cont++;
		for (i = 0; i < n; i++) {
			   cont++;
			count[ (arr[i]/exp)%10 ]++; 
		}

			   cont++;
		for (i = 1; i < 10; i++) {
			   cont++;
			count[i] += count[i - 1]; 
		}

			   cont++;
		for (i = n - 1; i >= 0; i--) { 
			   cont++;
			output[count[ (arr[i]/exp)%10 ] - 1] = arr[i]; 
			   cont++;
			count[ (arr[i]/exp)%10 ]--; 
		} 

		   cont++;
		for (i = 0; i < n; i++) {
			   cont++;
			arr[i] = output[i]; 
		}
	} 

	static void radixsort(int arr[], int n) {
		int m = getMax(arr, n); 

		   cont++;
		for (int exp = 1; m/exp > 0; exp *= 10) {
			   cont++;
			countSort(arr, n, exp); 
		}
	} 

	static void print(int arr[]) {
	} 


	public static void main (String[] args) {
		int arr[] = {170, 45, 75, 90, 802, 24, 2, 66}; 
		int n = arr.length; 
	      System.out.println("Radix");
		System.out.println("Antes -->->->->\n" + Arrays.toString(arr)); 
		radixsort(arr, n); 
		System.out.println("Depois -->->->->\n " + Arrays.toString(arr)); 
		
		System.out.println("Oprações básicas " + cont);
	} 
} 

