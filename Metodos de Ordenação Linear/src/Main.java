import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int arr[] = new int [10000];
		for (int i = 0; i < 10000; i++) {
			arr[i] = new Random().nextInt(10001);
		}
		
		int n = arr.length;

//		System.out.println("Antes -->->->->" + Arrays.toString(arr)); 
		System.out.println("\nRadixSort");
		RadixSort.radixsort(arr, n); 
//		System.out.println("Depois -->->->-> " + Arrays.toString(arr)); 
		System.out.println("Oprações básicas " + RadixSort.cont);

		System.out.println("\nHeapSort");
		HeapSort.heapSort(arr);
//		System.out.println("Depois -->->->-> " + Arrays.toString(arr)); 
		System.out.println("Oprações básicas " + HeapSort.cont);

		System.out.println("\nCountingSort");
		CountingSort.sort(arr);
//		System.out.println("Depois -->->->-> " + Arrays.toString(arr)); 
		System.out.println("Oprações básicas " + CountingSort.cont);

		System.out.println("\nBucketSort");
		BucketSort.bucket(arr, n);
//		System.out.println("Depois -->->->-> " + Arrays.toString(arr)); 
		System.out.println("Oprações básicas " + BucketSort.cont);
		
		
	}

}
