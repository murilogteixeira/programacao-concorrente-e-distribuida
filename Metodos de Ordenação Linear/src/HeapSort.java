import java.util.*;
 
public class HeapSort {
 
   private static int[] a;
   private static int n;
   private static int left;
   private static int right;
   private static int largest;
   static int cont = 0;
 
 
   public static void buildheap(int []a) {
      n = a.length-1;
      for(int i=n/2; i>=0; i--){
         maxheap(a,i);
      }
   }
 
   public static void maxheap(int[] a, int i) { 
      left = 2*i;
      right = 2*i+1;
 
      if(left <= n && a[left] > a[i]){
         largest=left;
      } else {
         largest=i;
      }
 
      if(right <= n && a[right] > a[largest]) {
         largest=right;
      }
      if(largest!=i) {
         exchange(i, largest);
         maxheap(a, largest);
      }
   }
 
   public static void exchange(int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t; 
   }
 
   public static void heapSort(int[] myarray) {
      a = myarray;
      buildheap(a);
	   cont++;
      for(int i=n; i>0; i--) {
   	   cont++;
         exchange(0, i);
  	   cont++;
         n=n-1;
  	   cont++;
         maxheap(a, 0);
      }
   }
 
   public static void main(String[] args) {
      int []numbers={55,2,93,1,23,10,66,12,7,54,3};
      System.out.println("Heap");
     System.out.println("Antes -->->->->\n " + Arrays.toString(numbers));
      heapSort(numbers);
      System.out.println("Depois -->->->->\n " + Arrays.toString(numbers));
      
      System.out.println("Operações básicas " + cont);
   }
}