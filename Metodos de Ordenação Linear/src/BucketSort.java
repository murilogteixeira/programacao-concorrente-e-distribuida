import java.util.*;
 
public class BucketSort{
	static int cont = 0;
 
   public static void bucket(int[] a, int n) {
      int [] bucket=new int[n+1];
 
	   cont++;
      for (int i=0; i<bucket.length; i++) {
   	   cont++;
         bucket[i]=0;
      }
 
	   cont++;
      for (int i=0; i<a.length; i++) {
   	   cont++;
         bucket[a[i]]++;
      }
 
      int pos=0;
      
	   cont++;
      for (int i=0; i<bucket.length; i++) {
   	   cont++;
         for (int j=0; j<bucket[i]; j++) {
      	   cont++;
            a[pos++]=i;
         }
      }
   }
 
 
   public static void main(String[] args) {
      int n=5;
      int [] arr= {5,3,0,2,4,1,0,5,2,3,1,4}; 
 
      System.out.println("Bucket");
      System.out.println("Antes -->->->->\n " + Arrays.toString(arr));
      bucket(arr,n);
      System.out.println("Depois -->->->->\n " + Arrays.toString(arr));
      System.out.println("Operações básicas " + cont);
   }
}