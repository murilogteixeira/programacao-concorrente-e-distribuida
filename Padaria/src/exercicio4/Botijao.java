package exercicio4;

public class Botijao {
	//ATRIBUTOS
	private int autonomiaMs;
	private int autonomiaMsRestante;
	
	//CONTRUTORES
	public Botijao(int minutos) {
		this.autonomiaMs = minutos * 60*1000;
		this.autonomiaMsRestante = this.autonomiaMs;
		System.out.println("Botijao - Criado com " + getAutonomiaMs() + "ms / " + getAutonomiaPercent() + "%.");
	}

	//METODOS
	public void usarBotijao(int tempo){
		this.autonomiaMsRestante -= tempo;
		System.out.println("Botijao - Usado por " + tempo +"ms."
				+ "Restante: " + getAutonomiaMsRestante() + "ms / " + getAutonomiaPercent() + "%");
		if(getAutonomiaPercent() <= 0){
			synchronized (this) {
				this.notifyAll();
			}
		}
	}
	
	//GETTERS AND SETTERS
	public int getAutonomiaMs() {
		return autonomiaMs;
	}

	public int getAutonomiaMsRestante() {
		return autonomiaMsRestante;
	}

	public int getAutonomiaPercent() {
		if(autonomiaMs == 0){
			return 0;
		}
		return autonomiaMsRestante * 100 / autonomiaMs;
	}
	
}
