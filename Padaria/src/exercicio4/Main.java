
package exercicio4;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Forno forno = new Forno(new Botijao(0));
		Gerente gerente = new Gerente(forno);
		gerente.start();
		
		for (int i =0; i < 3; i++) {
			new Padeiro("Padeiro " + (i + 1), forno).start();
		}
	}

}
