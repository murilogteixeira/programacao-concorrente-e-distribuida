package exercicio4;

import java.util.Random;

public class Padeiro extends Thread{
	//ATRIBUTOS
	private Forno forno;
	private Pao pao;
	
	//CONSTRUTORES
	public Padeiro(String nome, Forno forno) {
		super(nome);
		setForno(forno);
		System.out.println(getName() + " - Criado");
	}
	
	//METODOS
	@Override
	public void run() {
		super.run();
		while(true) {
			this.pao = fazerMassa();
			
			this.pao = assarMassa(this.pao);
		}
	}
	
	public Pao fazerMassa() {
		Pao pao = new Pao(new Random().nextInt(501) + 500);
		System.out.println(getName() + " - Fazendo a massa (" + pao.getPeso() + "g).");
		try {
			Padeiro.sleep(pao.getTempoFazer());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(getName() + " - Fez a massa (" + pao.getTempoFazer() + "ms).");
		return pao;
	}
	
	public Pao assarMassa(Pao pao) {
		Pao paoAssado = null;			
		synchronized (forno) {
			while(forno.getBotijao().getAutonomiaMsRestante() <= 0){
				this.esperar();
			}
			paoAssado = getForno().assarPao(this, pao);
		}
		return paoAssado;
	}
	
	public synchronized void esperar() {
		try {
			System.out.println(getName() + " - Esperando");
			this.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	//GETTERS AND SETTERS
	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}

	public Pao getPao() {
		return pao;
	}

	public void setPao(Pao pao) {
		this.pao = pao;
	}
	
}
