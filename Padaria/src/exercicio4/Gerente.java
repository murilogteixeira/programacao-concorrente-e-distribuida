package exercicio4;

public class Gerente extends Thread {
	//ATRIBUTOS
	private Forno forno;
	
	//CONTRUTORES
	public Gerente(Forno forno) {
		setName("Gerente");
		this.forno = forno;
		System.out.println(getName() + " - Criado");
	}
	
	//METODOS
	@Override
	public void run() {
		super.run();
		
		while(true){
			System.out.println("teste");
			while(forno.getBotijao().getAutonomiaPercent() > 0){
				//this.esperar();
			}
			
			trocarBotijao();
		}
	}
	
	public void trocarBotijao(){
		synchronized (forno) {
			this.forno.setBotijao(new Botijao(1));
		}
		synchronized (this) {
			this.notifyAll();
		}
		System.out.println("Gerente - Botijao instalado");
		
	}
	
	public synchronized void esperar() {
		try {
			System.out.println(getName() + " - Esperando");
			this.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	//GETTERS AND SETTERS
	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}
	
	
}
