package exercicio4;

public class Pao {
	private Boolean assado;
	private int peso;
	private int tempoFazer;
	private int tempoAssar;
	
	
	public Pao(int peso) {
		// TODO Auto-generated constructor stub
		this.assado = false;
		this.peso = peso;
		this.tempoFazer = this.peso * 5;
		this.tempoAssar = this.peso * 10;
	}

	//GETTERS AND SETTERS
	public Boolean getAssado() {
		return assado;
	}

	public void setAssado(Boolean assado) {
		this.assado = assado;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public int getTempoFazer() {
		return tempoFazer;
	}

	public void setTempoFazer(int tempoFazer) {
		this.tempoFazer = tempoFazer;
	}

	public int getTempoAssar() {
		return tempoAssar;
	}

	public void setTempoAssar(int tempoAssar) {
		this.tempoAssar = tempoAssar;
	}

}
