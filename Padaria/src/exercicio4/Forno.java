package exercicio4;

import java.util.Random;

public class Forno {
	//ATRIBUTOS
	private Botijao botijao;
	
	//CONSTRUTORES
	public Forno(Botijao botijao) {
		this.botijao = botijao;
		System.out.println("Forno - Criado");
	}

	//METODOS
	public Pao assarPao(Padeiro padeiro, Pao pao) {
		System.out.println(padeiro.getName() + " - Colocou a massa no forno.");
		System.out.println("Forno - Assando o pao do " + padeiro.getName());
		try {
			Thread.sleep(new Random().nextInt(10000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pao.setAssado(true);
		System.out.println("Forno - Pao do "  + padeiro.getName() + " assado (" + pao.getTempoAssar() + "ms).");
		
		this.botijao.usarBotijao(pao.getTempoAssar());
		if(this.botijao.getAutonomiaMsRestante() <= 0){
			this.notifyAll();
		}
		
		return pao;
	}

	//GETTERS AND SETTERS
	public Botijao getBotijao() {
		return botijao;
	}

	public void setBotijao(Botijao botijao) {
		this.botijao = botijao;
	}


	
	
	
}
