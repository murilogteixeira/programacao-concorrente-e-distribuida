package exercicio4.old1;

import java.util.Random;

public class Forno {
	//ATRIBUTOS
	//private Lock assando;
	private Botijao botijao;
	private int timer;
	private Gerente gerente;
	
	//CONSTRUTORES
	public Forno(Botijao botijao) {
		// TODO Auto-generated constructor stub
		//assando = new ReentrantLock();
		//botijao = new Botijao(5*60*1000);
		addBotijao(botijao);
	}

	//METODOS
	public Pao assarPao(Pao pao, Padeiro padeiro) {
		//assando.lock();
		setTimer(new Random().nextInt(pao.getPeso() * 10));
		System.out.println(padeiro.getName() + " - Colocou a massa no forno.");
		try {
			System.out.println("Forno - Assando o pao do " + padeiro.getName());
			int autonomiaRestante = consumirBotijao(pao.getTempoRestante());
			if (autonomiaRestante < 0) {
				System.out.println("Botijao - Acabou!");
				pao.setTempoRestante(autonomiaRestante);
				padeiro.setPriority(10);
				this.gerente.notify();
				return pao;
			}
			Thread.sleep(pao.getTempoRestante() * 10);
			pao.setAssado(true);
			System.out.println("Forno - Pao do "  + padeiro.getName() + " assado em " + pao.getPeso() * 10 / 1000.0 + "s.");
			System.out.println("Botijao - Autonomia restante de " + this.botijao.getAutonomiaRestante() + "%");
			padeiro.setPriority(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//assando.unlock();
		return pao;
	}

	public void addBotijao(Botijao botijao) {
		this.botijao = botijao;
	}
	
	public void removeBotijao() {
		this.botijao = null;
	}
	
	public int consumirBotijao(int qtd) {
		return getBotijao().usarBotijao(qtd);
	}

	//GETTERS AND SETTERS


	public Botijao getBotijao() {
		return botijao;
	}	

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public Gerente getGerente() {
		return gerente;
	}

	public void setGerente(Gerente gerente) {
		this.gerente = gerente;
	}

	
	
	
}
