package exercicio4.old1;

public class Botijao {
	//ATRIBUTOS
	private int autonomiaInicial;
	private int autonomiaRestante;
	
	//CONSTRUTORES
	public Botijao(int autonomiaInicial) {
		setAutonomiaInicial(autonomiaInicial);
		setAutonomiaRestante(autonomiaInicial);
		System.out.println("Botijao - Criado. Autononomia de " + getAutonomiaInicial() + "%");
	}
	
	//METODOS
	public int usarBotijao(int qtdUtilizada) {
		this.autonomiaRestante -= qtdUtilizada;
//		System.out.println("Botijao utilizado. Autonomia restante de " + getAutonomiaRestante() + "%.");
		return getAutonomiaRestante();
	}
	
	//GETTERS AND SETTERS
	public int getAutonomiaInicial() {
		return autonomiaInicial / autonomiaInicial * 100;
	}

	public void setAutonomiaInicial(int autonomiaInicial) {
		this.autonomiaInicial = autonomiaInicial;
	}

	public int getAutonomiaRestante() {
		return this.autonomiaRestante * 100 / this.autonomiaInicial;
	}

	public void setAutonomiaRestante(int autonomiaRestante) {
		this.autonomiaRestante = autonomiaRestante;
	}
	
}
