package exercicio4.old1;

public class Gerente extends Thread {
	Forno forno;

	public Gerente(Forno forno) {
		// TODO Auto-generated constructor stub
		super("Gerente");
		System.out.println("Gerente - Criado.");
		setForno(forno);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		System.out.println("Gerente - Trabalhando.");
		while(true){
			trocarBotijao(this.forno);
		}
	}
	
	public void esperar(){
		try {
			this.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void trocarBotijao(Forno forno){
		synchronized (this) {
			setForno(forno);
			if(forno.getBotijao().getAutonomiaRestante() <= 0){
				System.out.println("Gerente - Botijao trocado.");
				forno.addBotijao(new Botijao(5 * 60 * 1000));
				esperar();
			}else{
				System.out.println("Gerente - Esperando.");
				esperar();
			}
		}
	}

	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}
	
}
