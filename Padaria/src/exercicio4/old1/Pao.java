package exercicio4.old1;

public class Pao {
	private Boolean assado;
	private int peso;
	private int tempoPreparo;
	private int tempoRestante;
	
	public Pao(int peso, int tempoPreparo) {
		// TODO Auto-generated constructor stub
		setAssado(false);
		setPeso(peso);
		setTempoPreparo(tempoPreparo);
		setTempoRestante(tempoPreparo);
	}

	//GETTERS AND SETTERS
	public Boolean getAssado() {
		return assado;
	}

	public void setAssado(Boolean assado) {
		this.assado = assado;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public int getTempoPreparo() {
		return tempoPreparo;
	}

	public void setTempoPreparo(int tempoPreparo) {
		this.tempoPreparo = tempoPreparo;
	}

	public int getTempoRestante() {
		return tempoRestante;
	}

	public void setTempoRestante(int tempoRestante) {
		this.tempoRestante = tempoRestante;
	}

}
