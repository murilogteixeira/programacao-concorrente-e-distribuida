package exercicio4.old2;

import java.util.Random;

public class Padeiro extends Thread{
	//ATRIBUTOS
	private Forno forno;
	private Pao pao;
	
	//CONSTRUTORES
	public Padeiro(String nome, Forno forno) {
		// TODO Auto-generated constructor stub
		super(nome);
		setForno(forno);
		System.out.println(getName() + " - Criado");
	}
	
	//M�TODOS
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true) {
			setPao(fazerMassa());
			setPao(assarMassa(getPao()));
			setPao(null);
		}
	}
	
	public Pao fazerMassa() {
		if(this.pao != null){
			return pao;
		}
		Pao pao = null;
		int peso = new Random().nextInt(501) + 500;
		try {
			pao = new Pao(peso);
			System.out.println(getName() + " - Fazendo a massa de " + pao.getPeso() + "g.");
			Padeiro.sleep(pao.getTempoMassa());
			System.out.println(getName() + " - Fez a massa em " + pao.getTempoMassa() + "ms.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pao;
	}
	
	public Pao assarMassa(Pao pao) {
			return getForno().assarPao(pao, this);
	}
	
	public synchronized void descansar() {
		System.out.println(getName() + " - Descansando.");
		try {
			this.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//GETTERS AND SETTERS
	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}

	public Pao getPao() {
		return pao;
	}

	public void setPao(Pao pao) {
		this.pao = pao;
	}
	
}
