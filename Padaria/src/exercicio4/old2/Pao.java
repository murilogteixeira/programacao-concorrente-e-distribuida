package exercicio4.old2;

public class Pao {
	private Boolean assado;
	private int peso;
	private int tempoAssar;
	private int tempoMassa;
	
	public Pao(int peso) {
		// TODO Auto-generated constructor stub
		setAssado(false);
		setPeso(peso);
		setTempoAssar(peso*10);
		setTempoMassa(peso*5);
		System.out.println("Pao - Criado. " + getPeso() + "g");
	}

	//GETTERS AND SETTERS
	public Boolean getAssado() {
		return assado;
	}

	public void setAssado(Boolean assado) {
		this.assado = assado;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public int getTempoAssar() {
		return tempoAssar;
	}

	public void setTempoAssar(int tempoAssar) {
		this.tempoAssar = tempoAssar;
	}

	public int getTempoMassa() {
		return tempoMassa;
	}

	public void setTempoMassa(int tempoMassa) {
		this.tempoMassa = tempoMassa;
	}

}
