package exercicio4.old2;

public class Gerente extends Thread{
	//ATRIBUTOS
	private Forno forno;
	private Botijao botijao;
	
	//CONSTRUTORES
	public Gerente(Forno forno) {
		// TODO Auto-generated constructor stub
		System.out.println("Gerente - Criado.");
		this.forno = forno;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			System.out.println("teste");
			if(forno != null){
				System.out.println("Gerente - entrou no if ==================");
				trocarBotijao();
			}else{
				System.out.println("Gerente - entrou no else ==================");
				esperar();
				this.notifyAll();
			}
		}
	}

	//METODOS
	public void trocarBotijao(){
		synchronized (this) {
			System.out.println("Gerente - Trocando botijao.");
			this.botijao = new Botijao();
			this.forno.addBotijao(botijao);
			this.setPriority(5);
			this.notifyAll();
			this.forno = null;
		}
		//return botijao;
	}
	
	public void esperar(){
		synchronized (this) {
			
			try {
				System.out.println("Gerente - Descansando.");
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//GETTERS AND SETTERS

	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}

	public Botijao getBotijao() {
		return botijao;
	}

	public void setBotijao(Botijao botijao) {
		this.botijao = botijao;
	}
	
	
}
