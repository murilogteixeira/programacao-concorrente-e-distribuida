package exercicio4.old2;

public class Forno {
	//ATRIBUTOS
	private Botijao botijao;
	private Gerente gerente;
	
	//CONSTRUTORES
	public Forno(Botijao botijao, Gerente gerente) {
		// TODO Auto-generated constructor stub
		System.out.println("Forno - Criado.");
		this.botijao = botijao;
		this.gerente = gerente;
	}

	//METODOS
	public Pao assarPao(Pao pao, Padeiro padeiro) {
		synchronized (padeiro) {
			if(this.botijao.getAutonomiaRestante() <= 0){
				padeiro.descansar();
				return pao;
			}
		}
		
		synchronized (this) {

			
			int tempo = pao.getTempoAssar();
			System.out.println(padeiro.getName() + " - Colocou a massa no forno.");
			try {
				System.out.println("Forno - Assando o pao do " + padeiro.getName());
				
				this.botijao.usarBotijao(pao.getTempoAssar());
				
				int tempoAssarRestante = this.botijao.getAutonomiaRestante();
				
				if(tempoAssarRestante < 0){
					this.botijao.setAutonomiaRestante(0);
					tempoAssarRestante *= -1;
					pao.setAssado(false);
					pao.setTempoAssar(tempoAssarRestante);
					tempo -= tempoAssarRestante;

					this.gerente.setForno(this);
					this.gerente.notifyAll();
					System.out.println("Forno - passou por aqui.=========================");
				}else{
					pao.setAssado(true);
				}

				Thread.sleep(tempo);
				System.out.println("Forno - Pao do "  + padeiro.getName() + " assado em " + tempo + "ms.");				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pao;
	}

	//GETTERS AND SETTERS
	public Botijao getBotijao() {
		return botijao;
	}

	public void addBotijao(Botijao botijao) {
		this.botijao = botijao;
	}

	
	
	
	
}
