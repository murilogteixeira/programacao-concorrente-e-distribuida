package exercicio4.old2;

public class Botijao {
	//ATRIBUTOS
	private int autonomia;
	private int autonomiaRestante;
	//private Gerente gerente;
	
	//CONSTRUTORES
	public Botijao(/*Gerente gerente*/) {
		// TODO Auto-generated constructor stub
		this.autonomia = 1; // Minutos
		this.autonomia *= 60 * 1000;
		this.autonomiaRestante = autonomia;
		System.out.println("Botijao - Criado com autonomia de " + this.autonomiaRestante + "ms / " + this.autonomiaRestante * 100 / this.autonomia + "%");
		//this.gerente = gerente;
	}
	
	//METODOS
	public void usarBotijao(int qtdUso){
		
		this.autonomiaRestante -= qtdUso;
		System.out.println("Botijao - Utilizado.");
		System.out.println("Botijao - Autonomia restante de " + this.autonomiaRestante + "ms / " + this.autonomiaRestante * 100 / this.autonomia + "%");
		
		if(this.autonomiaRestante * 100 / this.autonomia < 15){
			System.out.println("Botijao - Acabando.");
		}
	}
	//-->>>botijao criado e funcionando. continuar com a substituicao quando terminar.
	//GETTERS AND SETTERS
	public int getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(int autonomia) {
		this.autonomia = autonomia;
	}

	public int getAutonomiaRestante() {
		return autonomiaRestante;
	}

	public void setAutonomiaRestante(int autonomiaRestante) {
		this.autonomiaRestante = autonomiaRestante;
	}

//	public Gerente getGerente() {
//		return gerente;
//	}
//
//	public void setGerente(Gerente gerente) {
//		this.gerente = gerente;
//	}
	
	
}
