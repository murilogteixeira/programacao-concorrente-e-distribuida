package exercicio2;

import java.util.Random;

public class Padeiro extends Thread{
	//ATRIBUTOS
	Forno forno;
	Pao pao;
	
	//CONSTRUTORES
	public Padeiro(String nome, Forno forno) {
		// TODO Auto-generated constructor stub
		super(nome);
		setForno(forno);
		System.out.println(getName() + " - Criado");
	}
	
	//M�TODOS
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true) {
			setPao(fazerMassa());
			setPao(assarMassa(getPao()));
		}
	}
	
	public Pao fazerMassa() {
		Pao pao = null;
		try {
			System.out.println(getName() + " - Fazendo a massa.");
			Padeiro.sleep(new Random().nextInt(5000));
			pao = new Pao();
			System.out.println(getName() + " - Fez a massa.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pao;
	}
	
	public Pao assarMassa(Pao pao) {
		synchronized (this.forno) {
			return getForno().assarPao(pao, this);
		}
	}
	
	public void descansar() {
		
	}

	//GETTERS AND SETTERS
	public Forno getForno() {
		return forno;
	}

	public void setForno(Forno forno) {
		this.forno = forno;
	}

	public Pao getPao() {
		return pao;
	}

	public void setPao(Pao pao) {
		this.pao = pao;
	}
	
}
