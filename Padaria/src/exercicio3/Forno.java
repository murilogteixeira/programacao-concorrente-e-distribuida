package exercicio3;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Forno {
	//ATRIBUTOS
	Lock assando;
	
	//CONSTRUTORES
	public Forno() {
		// TODO Auto-generated constructor stub
		assando = new ReentrantLock();
	}

	//METODOS
	public Pao assarPao(Pao pao, Padeiro padeiro) {
		assando.lock();
		System.out.println(padeiro.getName() + " - Colocou a massa no forno.");
		try {
			System.out.println("Forno - Assando o pao do " + padeiro.getName());
			Thread.sleep(new Random().nextInt(10000));
			pao.setAssado(true);
			System.out.println("Forno - Pao do "  + padeiro.getName() + " assado.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assando.unlock();
		return pao;
	}

	//GETTERS AND SETTERS

	
	
	
}
