package proj1;

public class Main {

	public static void main(String[] args) {
		int num = 10000;
		
		int meio = num/2;
		
		VerificaPrimo v1 = new VerificaPrimo(1, meio);
		VerificaPrimo v2 = new VerificaPrimo(meio+1, num);
		v1.start();
		v2.start();
		
		try {
			System.out.println("Aguardando Threads terminarem...");
			v1.join();
			v2.join();
		} catch (InterruptedException e) {
			System.out.println("Thread interrompida.");
		}
		
		int somaPrimos = v1.getSoma() + v2.getSoma();
		System.out.println(somaPrimos);
	}

}
