package proj1;

public class VerificaPrimo extends Thread{
	int soma = 0;
	int inicio = 0;
	int fim = 0;

	public VerificaPrimo (int inicio, int fim) {
		setInicio(inicio);
		setFim(fim);
	}
	
	@Override
	public void run() {
		super.run();
		for (int i = getInicio(); i <= getFim(); i++) {
			if(verificaPrimo(i)){
				setSoma(i);
			}
		}
	}
	
	public boolean verificaPrimo (int n) {
		for (int i = 2; i < n; i++) {
			if(n % i == 0){
				return false;
			}
		}
		return true;
	}
	
	public int getSoma(){
		return this.soma;
	}
	
	public void setSoma(int num){
		this.soma += num;
	}
	
	public int getInicio(){
		return this.inicio;
	}
	
	public void setInicio(int num){
		this.inicio = num;
	}
	
	public int getFim(){
		return this.fim;
	}
	
	public void setFim(int num){
		this.fim = num;
	}
	
}
