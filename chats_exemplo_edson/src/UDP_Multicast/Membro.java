package UDP_Multicast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTextArea;

public class Membro extends Thread {
	private static final int PORTA=12345;
	private MulticastSocket conexao;
	private InetAddress ipMC;
	private Membro clienteRecebe;
	private JTextArea taMensagens;
	private String nome; 

	public Membro() {
	}

	private Membro(MulticastSocket conexao, JTextArea taMensagens) {
		this.conexao = conexao;
		this.taMensagens = taMensagens;
	}

	public void run() {
		byte[] entrada=new byte[1000];
		while (true) {
			DatagramPacket dgEntrada = new DatagramPacket(entrada, entrada.length);
			try {
				this.conexao.receive(dgEntrada);
				this.taMensagens.insert((new String(entrada))+"\n", 0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean conectar(String host, String nome, JTextArea taMensagens) {
		this.nome = nome;
		try {
			this.ipMC = InetAddress.getByName(host);
			this.conexao = new MulticastSocket(Membro.PORTA);
			conexao.joinGroup(this.ipMC);
		} catch (IOException e) {
			return false;
		}
		(this.clienteRecebe = new Membro(conexao, taMensagens)).start();
		return true;
	}

	@SuppressWarnings("deprecation")
	public void desconectar() {
		try {
			this.clienteRecebe.stop();
			this.conexao.leaveGroup(this.ipMC);
			this.conexao.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void enviarMensagem(String mensagem) {
		DateFormat formato = new SimpleDateFormat("HH:mm");
		mensagem = this.nome +"("+ formato.format(new Date()) +"): "+ mensagem; 
		try {
			DatagramPacket dgSaida = new DatagramPacket(mensagem.getBytes(), mensagem.length(), this.ipMC, Membro.PORTA);
			this.conexao.send(dgSaida);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}