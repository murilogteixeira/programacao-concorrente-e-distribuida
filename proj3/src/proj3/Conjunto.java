package proj3;

public class Conjunto extends Thread{

	private long soma = 0;
	private long ini = 0;
	private long fim = 0;
	
	public Conjunto(long ini, long fim) {
		setIni(ini);
		setFim(fim);
	}
	
	@Override
	public void run() {
		super.run();
		somaPrimo(getIni(), getFim());
	}
	
	public void somaPrimo(long ini, long fim) {
		for (long i = getIni(); i <= getFim(); i++) {
			if (primo(i)) {
				setSoma(i);
			}
		}
	}
	
	private boolean primo(long num) {
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	public long getSoma() {
		return soma;
	}

	public void setSoma(long soma) {
		this.soma += soma;
	}

	public long getIni() {
		return ini;
	}

	public void setIni(long ini) {
		this.ini = ini;
	}

	public long getFim() {
		return fim;
	}

	public void setFim(long fim) {
		this.fim = fim;
	}

}
