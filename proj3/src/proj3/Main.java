package proj3;

import java.util.ArrayList;

public class Main {
	
	private static final int QTD_NUMEROS = 1000;
	private static final int QTD_THREADS = 4;

	public static void main(String[] args) {
		
		int parte = Main.QTD_NUMEROS/Main.QTD_THREADS;
		int ini = 0, fim = parte;
		long somaPrimos = 0;

		ArrayList<Conjunto> conjuntos = new ArrayList<Conjunto>();
		
		long iniTime = System.currentTimeMillis(), fimTime, time;
		
		for (int i = ini; i < Main.QTD_THREADS; i++) {
			ini += 1;
			System.out.println(ini + " " + fim);
			conjuntos.add(new Conjunto(ini, fim));
			ini = fim;
			fim += parte;
			
			if (i == Main.QTD_THREADS -2 && (fim - Main.QTD_NUMEROS) != 0) {
				fim += Main.QTD_NUMEROS - fim;
			}
		} 
		
		for (Conjunto conjunto : conjuntos) {
			conjunto.start();
			System.out.println(conjunto.getName());
		}
		
		try {
			for (Conjunto conjunto : conjuntos) {
				conjunto.join();
			}
		} catch (InterruptedException e) {
			System.out.println("Problema ao esperar threads. Erro: " + e.getMessage());
		}
		
		for (Conjunto conjunto : conjuntos) {
			somaPrimos += conjunto.getSoma();
		}
		
		fimTime = System.currentTimeMillis();
		time = fimTime - iniTime;
		
		System.out.println("Soma dos numeros primos: " + somaPrimos);
		System.out.println("Tempo decorrido: " + time + " milissegundos");
	}

}
