package Padrao;

import java.io.Serializable;
import java.util.Vector;

import Cliente.Cliente;

public class InfoJogo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<Cliente> jogadores;
//	private Integer somaJogadas;
	private Vector<Jogada> jogadas;
	private int totalPalitosJogo;
	private Cliente vencedor;
	
	public InfoJogo() {
		jogadores = new Vector<Cliente>();
//		somaJogadas = 0;
		jogadas = new Vector<Jogada>();
		totalPalitosJogo = 0;
	}

	public Vector<Jogada> getJogadas() {
		return jogadas;
	}

	public void setJogadas(Vector<Jogada> jogadas) {
		this.jogadas = jogadas;
	}

	public Cliente getVencedor() {
		for (Jogada jogada : jogadas) {
			if(jogada.getPalpite() == getSomaJogadas()) {
				return jogada.getJogador();
			}
		}
		return null;
	}

	public void setVencedor(Cliente vencedor) {
		this.vencedor = vencedor;
	}

	public Integer getSomaJogadas() {
		int soma = 0;
		for (Jogada jogada : jogadas) {
			soma += jogada.getPalitos();
		}
		return soma;
//		return somaJogadas;
	}

//	public void setSomaJogadas(Integer somaJogadas) {
//		this.somaJogadas = somaJogadas;
//	}

	public Vector<Cliente> getJogadores() {
		return jogadores;
	}

	public void setJogadores(Vector<Cliente> jogadores) {
		this.jogadores = jogadores;
	}

	public int getTotalPalitosJogo() {
		for (Cliente jogador : jogadores) {
			this.totalPalitosJogo += jogador.getPalitos();
		}
		return totalPalitosJogo;
	}

	public void setTotalPalitosJogo(int totalPalitosJogo) {
		this.totalPalitosJogo = totalPalitosJogo;
	}
	
}
