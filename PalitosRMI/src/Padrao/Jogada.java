package Padrao;

import java.io.Serializable;

import Cliente.Cliente;

public class Jogada implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int palpite;
	private int palitos;
	private Cliente jogador;
	
	public Jogada(Cliente jogador, int palpite, int palitos) {
		this.jogador = jogador;
		this.palpite = palpite;
		this.palitos = palitos;
	}

	public int getPalpite() {
		return palpite;
	}

	public void setPalpite(int palpite) {
		this.palpite = palpite;
	}

	public int getPalitos() {
		return palitos;
	}

	public void setPalitos(int palitos) {
		this.palitos = palitos;
	}

	public Cliente getJogador() {
		return jogador;
	}

	public void setJogador(Cliente jogador) {
		this.jogador = jogador;
	}
	
	
}
