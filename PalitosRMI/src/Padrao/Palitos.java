package Padrao;

import java.rmi.*;

import Cliente.Cliente;

public interface Palitos extends Remote {
	
	public Boolean entrarMesa(Cliente jogador) throws RemoteException;
	public InfoJogo jogar(Cliente jogador) throws RemoteException;
	public InfoJogo obterInfoJogo() throws RemoteException;
	public Cliente vencedor() throws RemoteException;
}
