package Cliente;

import java.io.Serializable;
import java.rmi.Naming;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import Padrao.*;

public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private Jogada jogada;
	private int palitos;
	private InfoJogo infoJogo;
	private Boolean venceu;
	
	public Cliente(String nome, InfoJogo infoJogo) {
		this.nome = nome;
		this.palitos = 3;
		this.infoJogo = infoJogo;
		this.venceu = false;
	}
	
	public static void main(String[] args) {
		try {
			Palitos palitos = (Palitos)Naming.lookup("rmi://localhost:1099/Palitos");

			Cliente murilo = new Cliente("Murilo", palitos.obterInfoJogo());
			Cliente erilane = new Cliente("Erilane", palitos.obterInfoJogo());
			Cliente jogador = new Cliente("Jogador", palitos.obterInfoJogo());

			Boolean muriloEntrou = palitos.entrarMesa(murilo);
			Boolean erilaneEntrou = palitos.entrarMesa(erilane);
			Boolean jogadorEntrou = palitos.entrarMesa(jogador);
			
			if(muriloEntrou && erilaneEntrou && jogadorEntrou) {
				murilo.newJogada();
				palitos.jogar(murilo);
				
				erilane.newJogada();
				palitos.jogar(erilane);
				
				jogador.newJogada();
				palitos.jogar(jogador);
			}
			
			InfoJogo infoJogo = palitos.obterInfoJogo();
			
			Cliente vencedor = infoJogo.getVencedor();
			if(vencedor != null) {
				System.out.println("Temos um vencedor!! Nome: " + vencedor.getNome() + ", Palpite: " + vencedor.getJogada().getPalpite());
			} else {
				System.out.println("Oh não. Ninguem venceu.");
			}
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Jogada getJogada() {
		return jogada;
	}

	public void newJogada() {
		int palitos = new Random().nextInt(this.palitos+1);
		int numJogadores = infoJogo.getJogadores().size()+1;
		int totalPalitosJogo = numJogadores * 3;
		int palpite = new Random().nextInt(totalPalitosJogo - palitos) + palitos;
//		int palpite = ThreadLocalRandom.current().nextInt(palitos, totalPalitosJogo);
		Jogada jogada = new Jogada(this, palpite, palitos);
		this.jogada = jogada;
	}

	public InfoJogo getInfoJogo() {
		return infoJogo;
	}

	public void setInfoJogo(InfoJogo infoJogo) {
		this.infoJogo = infoJogo;
	}

	public Boolean getVenceu() {
		
		return venceu;
	}

	public void setVenceu(Boolean venceu) {
		this.venceu = venceu;
	}

	public int getPalitos() {
		return palitos;
	}

	public void setPalitos(int palitos) {
		this.palitos = palitos;
	}
	
	

}
