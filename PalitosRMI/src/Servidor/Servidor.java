package Servidor;

import java.rmi.Naming;

public class Servidor {
	public static void main(String[] args) {
		try {
			PalitosRMI palitos = new PalitosRMI();
			Naming.rebind("rmi://localhost:1099/Palitos", palitos);
			System.out.println("Jogo Palitos Registrado.");
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}
}
