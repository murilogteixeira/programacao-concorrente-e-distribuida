package Servidor;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import Cliente.Cliente;
import Padrao.*;

public class PalitosRMI extends UnicastRemoteObject implements Palitos {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private InfoJogo infoJogo;

	protected PalitosRMI() throws RemoteException {
		super();
		infoJogo = new InfoJogo();
		
	}

	@Override
	public Boolean entrarMesa(Cliente novoJogador) throws RemoteException {
		for (Cliente jogador : infoJogo.getJogadores()) {
			if(jogador.getNome().equals(novoJogador.getNome())) {
				return false;
			}
		}
		infoJogo.getJogadores().add(novoJogador);
		return true;
	}

	@Override
	public InfoJogo jogar(Cliente jogador) throws RemoteException {
		this.infoJogo.getJogadas().add(jogador.getJogada());
//		System.out.println(jogador.getNome() + " jogou. Palitos: " + jogador.getJogada().getPalitos() + "Palpite: " + jogador.getJogada().getPalpite());
		System.out.println("Nome: "+jogador.getNome());
		System.out.println("Palito: "+jogador.getJogada().getPalitos());
		System.out.println("Palpite: "+jogador.getJogada().getPalpite());
		return infoJogo;
	}

	public InfoJogo getInfoJogo() {
		return infoJogo;
	}

	public void setInfoJogo(InfoJogo infoJogo) {
		this.infoJogo = infoJogo;
	}

	@Override
	public InfoJogo obterInfoJogo() throws RemoteException {
		// TODO Auto-generated method stub
		return infoJogo;
	}

	@Override
	public Cliente vencedor() throws RemoteException {
		for (Cliente jogador : infoJogo.getJogadores()) {
			if(jogador.getJogada().getPalpite() == infoJogo.getSomaJogadas()) {
				return jogador;
			}
		}
		return null;
	}
}
