package proj4;

import java.util.ArrayList;

public class Main {
	
	private static final int TAM_VETOR = 1000000;
	private static final int QTD_THREAD = 128;

	public static void main(String[] args) {
		int parte = Main.TAM_VETOR/Main.QTD_THREAD;
		int ini = -1, fim = parte -1;
		int maior = 0;
		long iniTime, fimTime, time;
		
		ArrayList<Integer> vetor = Utils.criarVetor(Main.TAM_VETOR);
		ArrayList<Vetor> vetores = new ArrayList<Vetor>();

		iniTime = System.currentTimeMillis();

		for (int i = 0; i < Main.QTD_THREAD; i++) {
			ini += 1;
			vetores.add(new Vetor(vetor, ini, fim));
			ini = fim;
			fim += parte;
			
			if(i == Main.QTD_THREAD -2 && Main.TAM_VETOR != fim +1){
				fim += Main.TAM_VETOR - fim;
			}
		}
		
		for (Vetor v : vetores) {
			v.start();
			//System.out.println(v.getName());
		}

		
		try {
			for (Vetor v : vetores) {
				v.join();
			}
		} catch (Exception e) {
			System.out.println("Erro: " + e);
		}
		
		for (Vetor vetor2 : vetores) {
			if(vetor2.getMaior() > maior){
				maior = vetor2.getMaior();
			}
		}

		fimTime = System.currentTimeMillis();
		time = fimTime - iniTime;
		
		System.out.println("\nMaior: " + maior);
		System.out.println("Tempo de execucao: " + time + " ms");
	}

}
