package proj4;

import java.util.ArrayList;

public class Vetor extends Thread{
		
	private ArrayList<Integer> vetor = new ArrayList<Integer>();
	private ArrayList<Integer> vetorTemp = new ArrayList<Integer>();
	private int maior;
	private int ini;
	private int fim;
	
	
	public Vetor(ArrayList<Integer> vetor, int ini, int fim) {
		
		setIni(ini);
		setFim(fim);
		setVetorTemp(vetor);
	}
	
	@Override
	public void run() {
		super.run();
		setVetor();
		verificaMaior();
	}
	
	private void verificaMaior(){
		int maior = 0;
		//System.out.println(getIni() + " " + getFim());
		for (int i = 0; i < getVetor().size(); i++) {
			if(getVetor().get(i)> maior){
				maior = getVetor().get(i);
			}
		}
		setMaior(maior);
	}

	public ArrayList<Integer> getVetor() {
		return vetor;
	}

	public void setVetor() {
		for (int i = getIni(); i < getFim(); i++) {
			this.vetor.add(getVetorTemp().get(i));
			//System.out.println(vetor.get(i));
		}
	}

	public ArrayList<Integer> getVetorTemp() {
		return vetorTemp;
	}

	public void setVetorTemp(ArrayList<Integer> vetorTemp) {
		this.vetorTemp = vetorTemp;
	}

	public int getIni() {
		return ini;
	}

	public void setIni(int ini) {
		this.ini = ini;
	}

	public int getFim() {
		return fim;
	}

	public void setFim(int fim) {
		this.fim = fim;
	}

	public int getMaior() {
		return maior;
	}

	public void setMaior(int maior) {
		this.maior = maior;
	}
	
	
}
