package proj4;

import java.util.ArrayList;
import java.util.Random;

public class Utils {

	public static Integer getRand(int limite) {		
		Random gerador = new Random();
		
		return gerador.nextInt(limite);
	}

	public static ArrayList<Integer> criarVetor(int tamVetor) {
		ArrayList<Integer> vetor = new ArrayList<Integer>();
		
		for(int i = 0; i < tamVetor; i++){
			vetor.add(Utils.getRand(tamVetor));
		}
		
		return vetor;
	}
	
}
