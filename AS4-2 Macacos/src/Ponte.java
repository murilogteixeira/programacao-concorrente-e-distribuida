import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ponte {
	public int contadorDireita;
	public int contadorEsquerda;
	public Lock botao;
	
	public Ponte() {
		// TODO Auto-generated constructor stub
		this.contadorDireita = 0;
		this.contadorEsquerda = 0;
		this.botao = new ReentrantLock();
		System.out.println("Ponte criada");
	}

	public void somarPlaca(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorDireita++;
				System.out.println("Placa lado direito: "+ this.contadorDireita);
				break;

			case "esq":
				System.out.println("Bot�o atravessar para lado " + irParaLado + " acionado");
				this.contadorEsquerda++;
				System.out.println("Placa lado esquerdo: "+ this.contadorEsquerda);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}

	public void subtrairPlaca(String irParaLado){
		this.botao.lock();
		try {
			switch (irParaLado) {
			case "dir":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorDireita--;
				System.out.println("Placa lado direito: "+ this.contadorDireita);
				break;

			case "esq":
				System.out.println("Bot�o atravessou para lado " + irParaLado + " acionado");
				this.contadorEsquerda--;
				System.out.println("Placa lado direito: "+ this.contadorEsquerda);
				break;

			default:
				System.out.println("Dados incorretos");
				break;
			}
		} finally {
			this.botao.unlock();
		}
	}

}

