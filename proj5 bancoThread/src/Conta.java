
public class Conta{
	private double saldo = 100;
	private double valor = 0;
	private boolean operacao = false;
	
	public Conta(double valor, boolean operacao) {
		// TODO Auto-generated constructor stub
		setValor(valor);
		setOperacao(operacao);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		
		
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public boolean isOperacao() {
		return operacao;
	}

	public void setOperacao(boolean operacao) {
		this.operacao = operacao;
	}

	public void sacar(double valor){
		this.saldo -= valor;
	}
	
	public void depositar(double valor){
		this.saldo += valor;
	}
	
	
}
