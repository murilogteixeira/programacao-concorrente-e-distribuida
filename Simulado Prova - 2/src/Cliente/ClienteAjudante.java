package Cliente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;

import natal.Mensagem;
import natal.Rodolfo;

public class ClienteAjudante {

	public static void main(String[] args) throws IOException, NotBoundException {
		// --> UDP
		// criar bytes de entrada e saida
		byte[] entradaUDP = new byte[1000], saidaUDP = new byte[1000];
		// criar pacotes de entrada e saida
		DatagramPacket dgEntrada = new DatagramPacket(entradaUDP, entradaUDP.length);
		DatagramPacket dgSaida = new DatagramPacket(saidaUDP, saidaUDP.length, InetAddress.getByName("localhost"), 12345);
		// criar socket
		DatagramSocket socketUDP = new DatagramSocket();
		// enviar saida
		socketUDP.send(dgSaida);
		// receber entrada
		socketUDP.receive(dgEntrada);
		String msg = new String(entradaUDP);
		String[] crianca = msg.split(";");
		System.out.println("Carta recebida da criança\n"
				+ "Nome: " + crianca[0] + ", Presente: " + crianca[1]);
		//fechar conexao
		socketUDP.close();
		
		// --> RMI
		Rodolfo rodolfo = (Rodolfo)Naming.lookup("rmi://localhost:1099/rodolfo");
		Boolean comportou = rodolfo.seComportou(crianca[0]);
		
		// --> TCP
		Socket socketTCP = new Socket("localhost", 12345);
		ObjectOutputStream saidaTCP = new ObjectOutputStream(socketTCP.getOutputStream());
		saidaTCP.writeObject(new Mensagem(crianca[0], comportou, crianca[1]));
		saidaTCP.close();
		socketTCP.close();
	}

}
