package Servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

import natal.Mensagem;

public class ServidorCrianca {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// --> UDP
		// criar entrada e saida
		String mensagem = "Murilo;Bike";
		byte[] entradaUDP = new byte[1000], saida = mensagem.getBytes();
		// criar socket
		DatagramSocket socketUDP = new DatagramSocket(12345);
		// criar pacote de entrada
		DatagramPacket dgEntrada = new DatagramPacket(entradaUDP, entradaUDP.length);
		// receber entrada
		socketUDP.receive(dgEntrada);
		// criar pacote de saida
		DatagramPacket dgSaida = new DatagramPacket(saida, saida.length, dgEntrada.getAddress(), dgEntrada.getPort());
		// enviar saida
		socketUDP.send(dgSaida);
		// fechar conexao
		socketUDP.close();
		
		// --> RMI
		
		// --> TCP
		ServerSocket servidor = new ServerSocket(12345);
		Socket socketTCP = servidor.accept();
		ObjectInputStream entradaTCP = new ObjectInputStream(socketTCP.getInputStream());
		Mensagem msg = (Mensagem)entradaTCP.readObject();
		System.out.println("Criança diz:\n"
				+ "Nome: " + msg.getNomeCrianca() + "\n"
				+ "Vai ganhar? " + msg.isVaiGanharPresente() + "\n"
				+ "Presente: " + msg.getPresenteDesejado());
		entradaTCP.close();
		servidor.close();
	}

}
