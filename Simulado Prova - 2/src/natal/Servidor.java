package natal;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class Servidor {

	public static void main(String[] args) throws RemoteException, MalformedURLException {
		ServidorRMI rodolfo = new ServidorRMI();
		Naming.rebind("rodolfo", rodolfo);
		System.out.println("Rodolfo registrado");
	}

}
