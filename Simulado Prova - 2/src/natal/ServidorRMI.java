package natal;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

public class ServidorRMI extends UnicastRemoteObject implements Rodolfo {

	protected ServidorRMI() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean seComportou(String nome) throws RemoteException {
		Boolean comportou = new Random().nextBoolean();
		System.out.println(nome + " comportou? " + comportou);
		return comportou;
	}

	

}
