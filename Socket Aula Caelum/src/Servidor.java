import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Servidor {

	public static void main(String[] args) throws IOException {
		// Abrir a porta
		ServerSocket servidor = new ServerSocket(12345);
		System.out.println("A porta 12345 está aberta.");
		
		// Esperar e aceitar um cliente
		Socket cliente = servidor.accept();
		System.out.println("Nova conexão com o cliente " + cliente.getInetAddress().getHostAddress());
		
		// Ler as informacoes que o cliente enviar
		Scanner s = new Scanner(cliente.getInputStream());
		while(s.hasNextLine()){
			System.out.println(s.nextLine());
		}
		
		s.close();
		servidor.close();
		cliente.close();

	}

}
