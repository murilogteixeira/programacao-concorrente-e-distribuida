import java.io.*;
import java.net.*;

import javax.swing.*;

public class Cliente extends Thread{
	private static int porta = 12345;
	private static String host = "localhost";
	private Socket conexao;
	private ObjectOutputStream saida;
	private Cliente clienteRecebe;
	private JTextArea taMensagens;
	
	public Cliente() {}
	
	public Cliente(Socket conexao, JTextArea taMensagens) {
		this.conexao = conexao;
		this.taMensagens = taMensagens;
	}
	
	public void run() {
		String mensagem;
		ObjectInputStream entrada = null;
		try {
			entrada = new ObjectInputStream(this.conexao.getInputStream());
			while(true) {
				mensagem = (String) entrada.readObject();
				this.taMensagens.insert(mensagem+"\n", 0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (ThreadDeath e) {
			try {
				entrada.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public boolean conectar(String nome, String taMensagens) {
		try {
			this.conexao = new Socket(host, porta);
			this.saida = new ObjectOutputStream(this.conexao.getOutputStream());
			this.saida.flush();
			this.saida.writeObject(taMensagens);
//			this.saida.writeObject(nome);
//			this.clienteRecebe = new Cliente(conexao, taMensagens);
//			this.clienteRecebe.start();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	public void desconectar() {
		try {
			this.saida.flush();
			this.saida.writeObject("CMD|DESCONECTAR");
			this.clienteRecebe.interrupt();
			this.saida.close();
			this.conexao.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void enviarMensagem(String mensagem) {
		try {
			this.saida.flush();
			this.saida.writeObject(mensagem);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
