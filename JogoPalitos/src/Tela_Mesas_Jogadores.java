
// Importa os pacotes necessários
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** CLASSE Tela_Mesas_Jogadores
* ------------------------------------
* Projeto..: Pacote
* Descrição: 
* Direitos.: Fernando Anselmo © mai - 2019
* Autor....: Cafeteira Versão 1.09 - Java - Gerador Automático
*/
public class Tela_Mesas_Jogadores extends JFrame {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
// Insira aqui os dados da Criação dos Objetos (bloco 1)
	  // Objetos da Janela (bloco 1)
	  private JTextField enderecoServidor = new JTextField("localhost");
	  private JTextField nomeJogador = new JTextField("Usuario");
	  private JButton btnConectar = new JButton("Conectar");
	  private JButton btnJogar = new JButton("Jogar");
	  private JButton btnLevantar = new JButton("Levantar");
	  private JButton btnSentar = new JButton("Sentar");
	  private JLabel lbServidor = new JLabel("Ende. Servidor");
	  private JLabel lbNomeUsuario = new JLabel("Nome Usuário");
	  private JList<String> mesas = new JList<String>();
	  private JList<String> jogadores = new JList<String>();

  public Tela_Mesas_Jogadores() {
    try {
      mostra();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public void mostra() throws Exception {

// Insira aqui os dados da Criação da Janela (bloco 2)
	// Dados da Janela (bloco 2)
    this.getContentPane().setLayout(null);
    this.getContentPane().setBackground(new Color(238, 238, 238));
    this.setSize(586, 459);
    this.setLocation(167, 180);
    this.setTitle("Cafeteira - Versão 1.09 - Java");
    this.setResizable(false);

// Insira aqui os dados da Criação dos Controles na Janela (bloco 3)
 // Cria os Objetos na Janela (bloco 3)
    enderecoServidor.setBounds(new Rectangle(9, 25, 100, 21));
    this.getContentPane().add(enderecoServidor, null);
    nomeJogador.setBounds(new Rectangle(112, 25, 100, 21));
    this.getContentPane().add(nomeJogador, null);
    btnConectar.setBounds(new Rectangle(212, 22, 100, 30));
    this.getContentPane().add(btnConectar, null);
    btnConectar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnJogar.setBounds(new Rectangle(323, 402, 100, 30));
    this.getContentPane().add(btnJogar, null);
    btnJogar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnLevantar.setBounds(new Rectangle(474, 403, 100, 30));
    this.getContentPane().add(btnLevantar, null);
    btnLevantar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnSentar.setBounds(new Rectangle(9, 402, 100, 30));
    this.getContentPane().add(btnSentar, null);
    btnSentar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    lbServidor.setBounds(new Rectangle(13, 9, 100, 13));
    this.getContentPane().add(lbServidor, null);
    lbNomeUsuario.setBounds(new Rectangle(117, 9, 100, 13));
    this.getContentPane().add(lbNomeUsuario, null);
    mesas.setBounds(new Rectangle(14, 58, 300, 340));
    this.getContentPane().add(mesas, null);
    jogadores.setBounds(new Rectangle(324, 60, 250, 340));
    this.getContentPane().add(jogadores, null);
    enderecoServidor.setBounds(new Rectangle(9, 25, 100, 21));
    this.getContentPane().add(enderecoServidor, null);
    nomeJogador.setBounds(new Rectangle(112, 25, 100, 21));
    this.getContentPane().add(nomeJogador, null);
    btnConectar.setBounds(new Rectangle(212, 22, 100, 30));
    this.getContentPane().add(btnConectar, null);
    btnConectar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnJogar.setBounds(new Rectangle(323, 402, 100, 30));
    this.getContentPane().add(btnJogar, null);
    btnJogar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnLevantar.setBounds(new Rectangle(474, 403, 100, 30));
    this.getContentPane().add(btnLevantar, null);
    btnLevantar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    btnSentar.setBounds(new Rectangle(9, 402, 100, 30));
    this.getContentPane().add(btnSentar, null);
    btnSentar.addMouseListener (new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        // Chamada ao Procedimento
    }});
    lbServidor.setBounds(new Rectangle(13, 9, 100, 13));
    this.getContentPane().add(lbServidor, null);
    lbNomeUsuario.setBounds(new Rectangle(117, 9, 100, 13));
    this.getContentPane().add(lbNomeUsuario, null);
    mesas.setBounds(new Rectangle(14, 58, 300, 340));
    this.getContentPane().add(mesas, null);
    jogadores.setBounds(new Rectangle(324, 60, 250, 340));
    this.getContentPane().add(jogadores, null);

    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        aoFechar();
      }
    });
  }

  private void aoFechar() {
    System.exit(0);
  }

  public static void main(String args[]) {
    (new Tela_Mesas_Jogadores()).setVisible(true);
  }
}