// http://www.mballem.com/post/chat-multi-usuarios-com-socket/?i=1

package testes;

import java.io.*;
import java.net.*;
import java.util.*;

public class Servidor extends Thread {
	private static Vector<PrintStream> clientes; // controla as conexoes
	private static ArrayList<String> listaNomes = new ArrayList<>(); // lista de nome dos clientes
	private static ServerSocket servidor;
	private Socket conexao; // socket do cliente
	private String nomeCliente; // nome do cliente
	
	// construtor que recebe o socket do cliente
	public Servidor(Socket socket) {
		this.conexao = socket;
	}
	
	// armazena o cliente se nao existir um ja criado
	// retorna true se existe e falso se nao existe
	public boolean armazenar(String novoCliente) {
		for (String s : listaNomes) {
			if(s.equals(novoCliente))
				return true;
		}
		listaNomes.add(novoCliente);
		return false;
	}
	
	// remove o cliente que deixar o chat
	public void remover(String antigoCliente){
		for (String s : listaNomes) {
			if(listaNomes.contains(s))
				listaNomes.remove(s);
		}
	}
	
	public static void main(String[] args) {
		clientes = new Vector<>(); // cria o vetor de clientes conectados
		try {
			servidor = new ServerSocket(12345);
			System.out.println("Servidor iniciado na porta 12345");
			while(true){
				Socket conexao = servidor.accept(); // aguarda cliente conectar
				new Servidor(conexao).start(); // cria uma thread para tratar essa conexao
			}
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			// objetos que permitem controlar a comunicacao do cliente
			BufferedReader entrada = new BufferedReader(new InputStreamReader(this.conexao.getInputStream()));
			PrintStream saida = new PrintStream(this.conexao.getOutputStream());
			// recebe o nome do cliente
			this.nomeCliente = entrada.readLine();
			// verifica se existe nome igual
			if(armazenar(this.nomeCliente)){
				saida.println("Nome ja existe! Conecte com outro nomee.");
				clientes.add(saida);
				this.conexao.close(); // fecha conexao
				return;
			} else {
				System.out.println(this.nomeCliente + ": Conectado ao servidor."); // mostra o nome do cliente conectado
			}
			
			// se o nome for igual a null a conexao eh encerrada
			if(this.nomeCliente == null){
				return;
			}
			
			// adiciona os dados de saida do cliente ao objeto clientes
			clientes.add(saida);
			// recebe a msg do cliente
			String msg = entrada.readLine();
			
			// se a linha = null, encerra conexao
			// senao mostra a troca de msgs
			while(msg != null && !(msg.trim().equals(""))){
				// enviar a msg para todos os clientes
				enviarParaTodos(saida, " escreveu: ", msg);
				// espera uma nova linha
				msg = entrada.readLine();
			}
			
			// se cliente enviar linha em branco, mostra no servidor que o cliente saiu
			System.out.println(this.nomeCliente + " saiu do chat");
			// se cliente enviar linha em branco, servidor avisa todos os clientes
			enviarParaTodos(saida, "saiu", " do chat!");
			
			// remove da lista
			remover(this.nomeCliente);
			// excluir atributos do cliente
			clientes.remove(saida);
			// fecha a conexao
			this.conexao.close();
		} catch (Exception e) {
			System.out.println("Falha na conexao... " + "IOException: " + e.getMessage());
		}
	}
	
	// enviar msg para todos os clientes, com excecao do proprio cliente
	public void enviarParaTodos(PrintStream saida, String acao, String msg) throws IOException {
		Enumeration<PrintStream> e = clientes.elements();
		while(e.hasMoreElements()) {
			// obtem o fluxo de saida de um dos clientes
			PrintStream chat = (PrintStream) e.nextElement();
			// envia para todos, excessao o proprio
			if(chat != saida){
				chat.println(this.nomeCliente + acao + msg);
			}
		}
	}
}
