// http://www.mballem.com/post/chat-multi-usuarios-com-socket/?i=1

package testes;

import java.io.*;
import java.net.*;

public class Cliente extends Thread {
	// controla recepcao de msgs dos clientes
	private Socket conexao;
	
	// contrutor recebe socket do cliente
	public Cliente(Socket socket) {
		this.conexao = socket;
	}

	public static void main(String[] args) {
		// instancia da conexao do tipo socket ao ip do servidor, porta
		Socket socket;
		try {
			socket = new Socket("localhost", 12345);
			
			// instancia o atributo de saida
			PrintStream saida = new PrintStream(socket.getOutputStream());
			
			// objetos que controlam o fluxo de saida
			BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Digite seu nome: ");
			String meuNome = teclado.readLine();
			
			// envia o nome para o servidor
			saida.println(meuNome.toUpperCase());
			
			// instancia a thread para o ip e porta conectados e inicia
			new Cliente(socket).start();
			
			// cria a variavel msg responsavel por enviar a mensagem para servidor
			String msg;
			
			while(true){
				// cria linha para digitar msg
				System.out.print("Mensagem> ");
				msg = teclado.readLine();
				
				// envia msg para servidor
				saida.println(msg);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Falha na conexao... " + "IOException: " + e.getMessage());
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			// recebe msgs de outro cliente atraves do servidor
			BufferedReader entrada = new BufferedReader(new InputStreamReader(this.conexao.getInputStream()));
			// cria variavel msg
			String msg;
			
			while(true){
				// pega o que o servidor enviou
				msg = entrada.readLine();
				
				// se a msg nao contiver dados encerra a conexao
				if(msg == null){
					System.out.println("Conexao encerrada!");
					System.exit(0);
				}
				
				System.out.println();
				// imprime msg recebida
				System.out.println(msg);
				// cria uma linha para resposta
				System.out.print("Responder> ");
			}
		} catch (IOException e) {
			System.out.println("Falha na conexao... " + "IOException: " + e.getMessage());
		}
	}
}
