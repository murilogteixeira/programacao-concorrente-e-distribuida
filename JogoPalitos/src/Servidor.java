import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class Servidor extends Thread {
	public static ArrayList<Integer> rodada = new ArrayList<>();
	
	private Socket conexao;
	private ArrayList<ObjectOutputStream> saidas;
	private static ServerSocket servidor;
	
	private Servidor(Socket conexao, ArrayList<ObjectOutputStream> saidas) {
		System.out.println("Cliente conectado: " + conexao.getInetAddress().getHostAddress());
		this.conexao = conexao;
		this.saidas = saidas;
	}
	
	public static void main(String[] args) {
		System.out.println("Servidor levantado...");
		ArrayList<ObjectOutputStream> saidas = new ArrayList<ObjectOutputStream>();
		try {
			servidor = new ServerSocket(12345);
			while(true) {
				Socket conexao = servidor.accept();
				(new Servidor(conexao, saidas)).start();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void run() {
		try {
			ObjectInputStream entrada = new ObjectInputStream(this.conexao.getInputStream());
//			ObjectOutputStream saida = new ObjectOutputStream(this.conexao.getOutputStream());
//			synchronized (this.saidas) {
//				this.saidas.add(saida);
//			}
			System.out.println(entrada.readObject());
//			String mensagem = (String) entrada.readObject();
//			super.setName(mensagem);
//			while(!(mensagem = (String) entrada.readObject()).equals("CMD|DESCONECTAR")) {
//				synchronized (this.saidas) {
//					for(ObjectOutputStream saida2 : this.saidas) {
//						saida2.flush();
//						saida2.writeObject(this.getName() + mensagem);
//					}
//				}
//			}
//			synchronized (this.saidas) {
//				this.saidas.remove(saida);
//			}
//			saida.close();
			entrada.close();
			System.out.println("Cliente desconectado: " + conexao.getInetAddress().getHostAddress());
			this.conexao.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
