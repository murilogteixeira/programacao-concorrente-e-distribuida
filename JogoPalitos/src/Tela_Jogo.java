// Importa os pacotes necessários
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** CLASSE Tela_Jogo
* ------------------------------------
* Projeto..: 
* Descrição: 
* Direitos.: Fernando Anselmo © mai - 2019
* Autor....: Cafeteira Versão 1.09 - Java - Gerador Automático
*/
public class Tela_Jogo extends JFrame {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Insira aqui os dados da Criação dos Objetos (bloco 1)
	  // Objetos da Janela (bloco 1)
	  private JLabel lbNomeMesa = new JLabel("Nome da Mesa:");
	  private JLabel nomeMesa = new JLabel("nomeMesa");
	  private JComboBox palpites = new JComboBox();
	  private JComboBox jogo = new JComboBox();
	  private JButton btnJogar = new JButton("Jogar");
	  private JList txtArea_Jogos_Palpites = new JList();
	  private JButton btnNovoJogo = new JButton("Novo Jogo");

  public Tela_Jogo() {
    try {
      mostra();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public void mostra() throws Exception {

// Insira aqui os dados da Criação da Janela (bloco 2)
	// Dados da Janela (bloco 2)
	    this.getContentPane().setLayout(null);
	    this.getContentPane().setBackground(new Color(238, 238, 238));
	    this.setSize(431, 436);
	    this.setLocation(167, 180);
	    this.setTitle("Cafeteira - Versão 1.09 - Java");
	    this.setResizable(false);

// Insira aqui os dados da Criação dos Controles na Janela (bloco 3)
	// Cria os Objetos na Janela (bloco 3)
	    lbNomeMesa.setBounds(new Rectangle(26, 17, 100, 13));
	    this.getContentPane().add(lbNomeMesa, null);
	    nomeMesa.setBounds(new Rectangle(134, 18, 100, 13));
	    this.getContentPane().add(nomeMesa, null);
	    palpites.setBounds(new Rectangle(309, 81, 100, 20));
	    this.getContentPane().add(palpites, null);
	    jogo.setBounds(new Rectangle(308, 43, 100, 20));
	    this.getContentPane().add(jogo, null);
	    btnJogar.setBounds(new Rectangle(307, 124, 100, 30));
	    this.getContentPane().add(btnJogar, null);
	    btnJogar.addMouseListener (new MouseAdapter() {
	      public void mouseClicked(MouseEvent e) {
	        // Chamada ao Procedimento
	    }});
	    txtArea_Jogos_Palpites.setBounds(new Rectangle(24, 42, 250, 350));
	    this.getContentPane().add(txtArea_Jogos_Palpites, null);
	    btnNovoJogo.setBounds(new Rectangle(307, 364, 100, 30));
	    this.getContentPane().add(btnNovoJogo, null);
	    btnNovoJogo.addMouseListener (new MouseAdapter() {
	      public void mouseClicked(MouseEvent e) {
	        // Chamada ao Procedimento
	    }});

    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        aoFechar();
      }
    });
  }

  private void aoFechar() {
    System.exit(0);
  }

  public static void main(String args[]) {
    (new Tela_Jogo()).setVisible(true);
  }
}