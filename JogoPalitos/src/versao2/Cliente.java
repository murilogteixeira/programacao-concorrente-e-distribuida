package versao2;

import java.io.*;
import java.net.*;

public class Cliente extends Thread implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String host = "localhost";
	private static int port = 12346;
	private Socket conexao;
	private String nome;
	
	public Cliente(String nome, Socket socket) {
		this.nome = nome;
		this.conexao = socket;
	}

	public static void main(String[] args) throws IOException {
		Socket socket;
		socket = new Socket(host, port);
		
		BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Digite seu nome: ");
		String nome = teclado.readLine();
		
		Cliente cliente = new Cliente(nome.toUpperCase(), socket);
		cliente.start();
	}
	
	public void novaJogada() {
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			ObjectOutputStream saida;
			ObjectInputStream entrada;
			saida = new ObjectOutputStream(this.conexao.getOutputStream());
			entrada = new ObjectInputStream(this.conexao.getInputStream());
			
			Mensagem objeto = new Mensagem("NOVO JOGADOR", this.nome, this);
			
			saida.writeObject(objeto);
			
//			while(true) {
//					
//				
//				
//			}
			
			this.conexao.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Erro: " + e.getMessage());
			e.printStackTrace();
		}
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
