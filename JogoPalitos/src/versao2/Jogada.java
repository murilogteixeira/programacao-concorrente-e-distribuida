package versao2;

public class Jogada {
	private int palitos;
	private int palpite;
	
	public Jogada() {
		this.palitos = 3;
		this.palpite = 0;
	}

	public int getPalitos() {
		return palitos;
	}

	public void setPalitos(int palitos) {
		this.palitos = palitos;
	}

	public int getPalpite() {
		return palpite;
	}

	public void setPalpite(int palpite) {
		this.palpite = palpite;
	}
	
	
}
