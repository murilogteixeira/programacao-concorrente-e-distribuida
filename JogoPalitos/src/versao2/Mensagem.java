package versao2;

public class Mensagem {
	private String comando;
	private Object objeto;
	private Cliente jogador;
	
	public Mensagem(String comando, Object objeto, Cliente jogador) {
		this.comando = comando;
		this.objeto = objeto;
		this.jogador = jogador;
	}
	
	public Cliente getJogador() {
		return jogador;
	}

	public void setJogador(Cliente jogador) {
		this.jogador = jogador;
	}
	
	public String getComando() {
		return comando;
	}
	
	public void setComando(String cmd) {
		this.comando = cmd;
	}
	
	public Object getObjeto() {
		return objeto;
	}
	
	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}
	
	
}
