package versao2;

import java.util.ArrayList;

public class Jogo {
	private int somaPalitos;
	private ArrayList<Integer> palpites;
	
	public int getSomaPalitos() {
		return somaPalitos;
	}
	
	public void resetPalitos() {
		this.somaPalitos = 0;
	}
	
	public void somarPalitos(int qtdPalitos) {
		this.somaPalitos += qtdPalitos;
	}
	
	public ArrayList<Integer> getPalpites() {
		return palpites;
	}
	
	public void resetPalpites() {
		this.palpites = new ArrayList<Integer>();
	}
	
	public void addPalpites(Integer palpite) {
		this.palpites.add(palpite);
	}
}
