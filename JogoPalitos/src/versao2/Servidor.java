package versao2;

import java.io.*;
import java.net.*;
import java.util.*;

public class Servidor extends Thread implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int host = 12346;
	private static ServerSocket servidor;
	private static String nomeCliente;
	private static ArrayList<Cliente> jogadores;
	private static Vector<ObjectOutputStream> saidas;
	private Socket conexao;
	private static Jogo infoJogo;
	
	public Servidor(Socket conexao){
		this.conexao = conexao;
	}

	public static void main(String[] args) {
		jogadores = new ArrayList<>();
		saidas = new Vector<ObjectOutputStream>();
		
		try {
			servidor = new ServerSocket(host);
			System.out.println("Servidor iniciado na porta " + host);
			
			while(true) {
				if(jogadores.size() < 3) {
					Socket conexao = servidor.accept();
					System.out.println("Conexao na porta " + conexao.getInetAddress().getHostAddress() + " "
							+ "Porta " + conexao.getPort());
					
					new Servidor(conexao).start();
				}
				enviarTodos(infoJogo);
			}
			
		} catch (IOException e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		
		try {
			ObjectInputStream entrada = new ObjectInputStream(this.conexao.getInputStream());
			ObjectOutputStream saida = new ObjectOutputStream(this.conexao.getOutputStream());
			
			Mensagem objeto = (Mensagem) entrada.readObject();
			
			switch (objeto.getComando()) {
			case "NOVO JOGADOR":
				if(!novoCliente(objeto, saida)){
					this.conexao.close();
					System.out.println(objeto.getJogador().getNome() + " já existe.");
					return;
				}else {
					System.out.println(objeto.getJogador().getNome() + " adicionado.");
				}
				break;
			case "NOVA JOGADA":
				novaJogada(objeto);
				break;
			case "RESPOSTA JOGO":
				repostaJogo(objeto);
				break;

			default:
				break;
			}
			
			nomeCliente = (String) entrada.readObject();
			
			saida.writeObject(nomeCliente + " conectado ao servidor");
			System.out.println(nomeCliente + " conectado. Endereço " + conexao.getInetAddress().getHostAddress() + " - Porta " + conexao.getLocalPort());
//			
//			conexao.close();
//			entrada.close();
//			saida.close();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private Boolean novoCliente(Mensagem objeto, ObjectOutputStream saida) {
		for (Cliente jogador : jogadores) {
			if(jogador.getNome().equals(objeto.getJogador().getNome())) {
				return false;
			}
		}
		saidas.add(saida);
		return true;
	}
	
	private void novaJogada(Mensagem objeto) {
		Jogada jogada = (Jogada)objeto.getObjeto();
		infoJogo.addPalpites(jogada.getPalpite());
		infoJogo.somarPalitos(jogada.getPalitos());
	}
	
	private void repostaJogo(Mensagem objeto) {
		String resposta = (String) objeto.getObjeto();
		if(resposta == "VENCEU"){
			System.out.println(objeto.getJogador().getNome() + " " + resposta + "!");
		}
	}
	
	private static void enviarTodos(Object object) throws IOException {
		Enumeration<ObjectOutputStream> e = saidas.elements();
		
		while(e.hasMoreElements()) {
			ObjectOutputStream envio = (ObjectOutputStream) e.nextElement();
			
			envio.writeObject(object);
		}
	}

}
