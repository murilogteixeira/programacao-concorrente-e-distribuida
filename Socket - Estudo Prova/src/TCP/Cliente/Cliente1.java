package TCP.Cliente;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Cliente1 {

	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 12345);
			ObjectInputStream entrada = new ObjectInputStream(socket.getInputStream());
			System.out.println("Mensagem: " + entrada.readObject());
			entrada.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
