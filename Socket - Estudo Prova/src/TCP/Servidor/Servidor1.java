package TCP.Servidor;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor1 {

	public static void main(String[] args) {
		String mensagem = "Hello World";
		try {
			ServerSocket servidor = new ServerSocket(12345);
			Socket socket = servidor.accept();
			System.out.println("Conexao com cliente: "
					+ socket.getInetAddress().getHostAddress()
					+ ":"
					+ socket.getPort());
			ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
			saida.writeObject(mensagem);
			saida.close();
			socket.close();
			servidor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
