package RMI.Servidor;

import java.rmi.Naming;

import RMI.Default.HelloRMI;

public class Servidor {

	public static void main(String[] args) {
		try {
			HelloRMI hello = new HelloRMI();
			Naming.rebind("hello", hello);
			System.out.println("Registrado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
