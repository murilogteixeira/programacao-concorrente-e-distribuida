package RMI.Default;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloRMI extends UnicastRemoteObject implements Hello {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HelloRMI() throws RemoteException {
		super();
	}

	@Override
	public String dizer() throws RemoteException {
		return "Hello World";
	}	

}
