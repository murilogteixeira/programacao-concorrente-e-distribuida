package RMI.Default;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

public class RMI_Registry {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws RemoteException {
		LocateRegistry.createRegistry(1099);
		System.out.println("RMI Registry iniciado");
		new Scanner(System.in).nextInt();
		System.out.println("RMI Registry encerrado");
	}

}
