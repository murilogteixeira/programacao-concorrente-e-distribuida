package RMI.Cliente;

import java.rmi.Naming;

import RMI.Default.Hello;

public class Cliente {

	public static void main(String[] args) {
		try {
			Hello hello = (Hello)Naming.lookup("hello");
			String mensagem = hello.dizer();
			System.out.println("Mensagem: " + mensagem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
