package UDP.Cliente;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Cliente2{
	public static void main(String[] args) {
		String mensagem;
		byte[] saida="OK".getBytes(), entrada=new byte[1000]; try {
			DatagramSocket socket = new DatagramSocket();
			DatagramPacket dgSaida = new DatagramPacket(saida, saida.length, InetAddress.getByName("localhost"), 12346);
			DatagramPacket dgEntrada = new DatagramPacket(entrada, entrada.length);
			socket.send(dgSaida);
			do {
				socket.receive(dgEntrada);
				mensagem = (new String(entrada)).trim();
				if (!mensagem.equals("CMD|DESCONECTAR")) {
					System.out.println("CLIENTE: "+ mensagem);
					socket.send(dgSaida);
				}
			} while (!mensagem.equals("CMD|DESCONECTAR"));
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 
}
