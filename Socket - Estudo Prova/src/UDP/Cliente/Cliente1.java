package UDP.Cliente;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class Cliente1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte[] saida = "Hello World".getBytes(), entrada = new byte[1000];
		try {
			DatagramSocket socket = new DatagramSocket();
			socket.setSoTimeout(3000);
			DatagramPacket dgSaida = new DatagramPacket(saida, saida.length, InetAddress.getByName("localhost"), 12345);
			DatagramPacket dgEntrada = new DatagramPacket(entrada, entrada.length);
			socket.send(dgSaida);
			socket.receive(dgEntrada);
			System.out.println("Mensagem: " + new String(entrada));
			socket.close();
		} catch (SocketTimeoutException e) {
			System.out.println("Tempo excedido");
		} catch (IOException e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
