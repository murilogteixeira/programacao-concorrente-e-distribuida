package UDP.Servidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Servidor2 {
	public static void main(String[] args) {
		String mensagem="Hello world!";
		byte[] entrada=new byte[1000], saida;
		try {
			DatagramSocket socket = new DatagramSocket(12346);
			DatagramPacket dgSaida, dgEntrada = new DatagramPacket(entrada, entrada.length); socket.receive(dgEntrada);
			System.out.println("SERVIDOR: "+ (new String(entrada)));
			for (int i=0; i<mensagem.length(); i++) {
				saida = String.valueOf(mensagem.charAt(i)).getBytes();
				dgSaida = new DatagramPacket(saida, saida.length, dgEntrada.getAddress(), dgEntrada.getPort());
				socket.send(dgSaida);
				socket.receive(dgEntrada);
				System.out.println("SERVIDOR: "+ (new String(entrada)));
			}
			saida = "CMD|DESCONECTAR".getBytes();
			dgSaida = new DatagramPacket(saida, saida.length, dgEntrada.getAddress(), dgEntrada.getPort());
			socket.send(dgSaida);
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
