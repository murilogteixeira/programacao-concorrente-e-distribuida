package UDP.Servidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Servidor1 {

	public static void main(String[] args) {
		// Mensagem para envio
		String mensagem = "OK.";
		// entrada = bytes de entrada/ saida = mensagem
		byte[] entrada = new byte[1000], saida = mensagem.getBytes();
		try {
			// Cria conexao
			DatagramSocket socket = new DatagramSocket(12345);
			System.out.println("Servidor iniciado");
			// Cria o pacote de entrada
			DatagramPacket dgEntrada = new DatagramPacket(entrada, entrada.length);
			// Recebe o pacote de entrada
			socket.receive(dgEntrada);
			// Mostra na tela quem conectou
			System.out.println("Conexão com cliente "
					+ dgEntrada.getAddress().getHostAddress()
					+ ":"
					+ dgEntrada.getPort());
			// Mostra na tela a mensagem
			System.out.println("Mensagem: " + new String(entrada));
			// Cria o pacote de saida
			DatagramPacket dgSaida = new DatagramPacket(saida, saida.length, dgEntrada.getAddress(), dgEntrada.getPort());
			socket.send(dgSaida);
			socket.close();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

}
