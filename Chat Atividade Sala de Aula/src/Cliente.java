import java.io.*;
import java.net.*;
import java.util.Random;

public class Cliente extends Thread {
	private static String host = "10.47.48.100";
	private static int port = 12345;
	private static int timeout = 1000;
	
	public static void main(String[] args) {
		Socket socket;
		try {
			socket = new Socket(host, port);
//			socket.setSoTimeout(timeout);
//			System.out.println(socket.getSoTimeout());
			ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream entrada = new ObjectInputStream(socket.getInputStream());

			Integer impar = 0;
			while(impar % 2 != 1) {
				impar = new Random().nextInt(11);
			}
			int msgEntrada = (int) entrada.readObject();
			System.out.println("Numero carlos: "+msgEntrada);
			saida.flush();
			saida.writeObject(impar);
			System.out.println("Meu numero: " + impar);
			
			String res = (String) entrada.readObject();
			
			if((msgEntrada + impar) % 2 == 0) {
				System.out.println(res); 
			} else {
				//res = "Murilo ganhou!!";
				//saida.flush();
				//saida.writeObject(res);
				//System.out.println(res);
			}
			
			socket.close();
			entrada.close();
			saida.close();
		} catch (IOException e) {
			System.out.println("Erro: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Erro: " + e.getMessage());
		}
	}
}
