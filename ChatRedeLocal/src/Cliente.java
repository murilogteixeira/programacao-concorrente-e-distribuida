import java.io.*;
import java.net.*;

public class Cliente {
	private static String host = "localhost";
	private static int port = 12345;
	
	public static void main(String[] args) {
		Socket socket;
		try {
			socket = new Socket(host, port);
			ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream entrada = new ObjectInputStream(socket.getInputStream());

			BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Digite seu nome: ");
			String meuNome = teclado.readLine();
			
			saida.writeObject(meuNome.toUpperCase());
			
			System.out.println(entrada.readObject());
			
			socket.close();
			entrada.close();
			saida.close();
		} catch (IOException e) {
			System.out.println("Erro: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}
}
