import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
	private static int host = 12345;
	private static ServerSocket servidor;
	private static String nomeCliente;
	
	public static void main(String[] args) {
		try {
			servidor = new ServerSocket(host);
			System.out.println("Servidor iniciado na porta " + host);
			Socket conexao = servidor.accept();
			ObjectInputStream entrada = new ObjectInputStream(conexao.getInputStream());
			ObjectOutputStream saida = new ObjectOutputStream(conexao.getOutputStream());
			nomeCliente = (String)entrada.readObject();
			saida.writeObject(nomeCliente + " conectado ao servidor.");
			System.out.println(nomeCliente + " conectou ao servidor.");
			conexao.close();
			entrada.close();
			saida.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Ërro: " + e.getMessage());
		}
	}
}
